#! /bin/bash

if [ "$1" == "-h" ]; then
	cat << EOF
		The first argument should the directory to put the output folder

		-c num     Number of civs (Must be first)
		-S num...  Strategies for each civ (i.e. 0.0 1.0 0.0 1.0 0.0 0.0)
		-p num...  Initial number of people for each civ
   		-k num 	   The starting K
		-T int     Timesteps, if not included, GUI starts

	To select update methods use. (At least one is needed)
		-lv	   Logistic growth equation
		-bdc	   Alterted Logistic growth where competition is separated from birth.
		-updatek   Collapse paper type update rule
EOF
	exit
fi

FOLDER=$1
FILE="b-$(date +"%d%m%y-%H%M%S-%N")"
shift

cd $FOLDER
mkdir $FILE
cd $FILE
echo $@ > args

cd ~/workspace/population/bin/
java run/ModelAbstractionTest $@ -f ${FOLDER}${FILE}/output.dat

#temp
exit

gnuplot44 -persist << EOF
	e=`tail -1 k_vs_time.dat | awk '{print NF}'`
	unset key
	plot for[i=2:e] "k_vs_time.dat" u 1:i w l
EOF

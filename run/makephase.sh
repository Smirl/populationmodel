#! /bin/bash

#for i in 1 0.5 0.1 0.05 0.01 0.005 0.001 0.0005 0.0001; do
for i in 0.082 0.084 0.086 0.088; do
	for j in 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95 1.0; do
		echo -n "lambda=$i "
		echo -n "B=$j "
		k=`echo "1.0 - $j" | bc`
		echo "A=$k "

		#Birthrate v Aggression
		qsub -V -cwd -pe smp 4 ./basic.sh ../output/ba-phase-2/ -c 2 -S $j 0 $k $k 0 $j -p 25 25 -k 100 -T 5000000 -q -param lambda=$i

		#Birthrate v Intellect
		#qsub -V -cwd -pe smp 4 ./basic.sh ../output/bi-phase/ -c 2 -S $j $k 0 $k $j 0 -p 25 25 -k 100 -T 5000000 -q -param lambda=$i

		#Intellect v Aggression
		#qsub -V -cwd -pe smp 4 ./basic.sh ../output/ia-phase/ -c 2 -S 0 $j $k 0 $k $j -p 25 25 -k 100 -T 5000000 -q -param lambda=$i
	done
done
exit

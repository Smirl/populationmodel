#! /bin/bash

#HELP MESSAGE
usage()
{
cat << EOF
usage: $0 options
***********************************************************************************

Run the population model with the below options. For defaults see -man output below.

OPTIONS:
   -h     	Show this message
   -o folder 	Output folder location relative to ~/workspace/population/bin/ 
		(~/workspace/population/output/PopVTime/ by default)
   -t n		Threads
   -S 		Square map (TRUE by DEFAULT)
   -I		Island based map (FALSE by DEFAULT)
   -A		Arena based map (one populated site in corners)
   -U		Map of the UK is used as map
   -l val	Length of the side of the square map
   -T n		Number of steps
   -c n		Number if civs
   -s n 	Seed for map generator
   -d val 	Density of islands for map generator
   -b	   	Do not save model file at end of run
   -p key=val	Set the value of a parameter "key" to "val"

EOF
}
#Go to the run directory
cd ~/workspace/population/bin/

#VARIABLES AND DEFAULTS
BASEFOLDER="../output/PopVTime/"
FOLDER="pt-$(date +"%d%m%y-%H%M%S-%N")"
THREAD="-t 500"
TYPE="-square"
LEN="-l 100"
TIME="-T 10000"
CIVS=
SEED=
DEN=
BINARY=
PARAM=

#OPTION PARSING
while getopts “ho:t:SIAUl:T:c:s:d:bp:” OPTION; do
     case $OPTION in
         h)
             usage
	     exit 1
             ;;
         o)
             BASEFOLDER="$OPTARG"
             ;;
	 t)
             THREAD="-t $OPTARG"
             ;;
	 S)
             TYPE="-square"
             ;;
	 A)
	     TYPE="-arena"
	     ;;
	 I)
             TYPE="-island"
             ;;
	 U)
	     TYPE="-uk"
	     ;;
	 l)
             LEN="-l $OPTARG"
             ;;
	 T)
             TIME="-T $OPTARG"
             ;;
	 c)
             CIVS="-c $OPTARG"
             ;;
	 s)
             SEED="-s $OPTARG"
             ;;
	 d)
             DEN="-d $OPTARG"
             ;;
	 p)
	     PARAM="-p $OPTARG"
	     ;;
	 b)
	     BINARY="-b"
	     ;;
         ?)
             usage
	     exit
             ;;
     esac
done

FILE="-f ${BASEFOLDER}${FOLDER}/output.dat"
FOLDER="${BASEFOLDER}${FOLDER}"

mkdir $FOLDER

#Run the model
java -Xmx512m -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/phys/linux/s0941897/workspace/population/dump.hprof run/PMPopVTime $FILE $THREAD $TYPE $LEN $TIME $CIVS $SEED $BINARY $DEN $PARAM

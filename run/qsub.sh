#! /bin/bash

#submit a job using qsub with the below options
#the job and all it's options are passed in

qsub -V -cwd -pe smp 16 $@;

#! /bin/bash

if [ "$1" == "-h" ]; then
	cat << EOF
		-c num     Number of civs (Must be first)
		-S num...  Strategies for each civ (i.e. 0.0 1.0 0.0 1.0 0.0 0.0)
		-p num...  Initial number of people for each civ
   		-k num 	   The starting K
		-T int     Timesteps, if not included, GUI starts

	To select update methods use. (At least one is needed)
		-lv	   Logistic growth equation
		-bdc	   Alterted Logistic growth where competition is separated from birth.
		-updatek   Collapse paper type update rule
EOF
	exit
fi


cd ~/workspace/population/bin/
java run/ModelAbstractionTest $@

gnuplot44 -persist << EOF
	e=`tail -1 basicDefaults.dat | awk '{print NF}'`
	unset key
	plot for[i=2:e] "basicDefaults.dat" u 1:i w l
EOF

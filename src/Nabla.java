import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Nabla extends JFrame implements KeyListener, MouseListener{

	private static final long serialVersionUID = 1L;

	public static void main(String[] args){
		new Nabla();
	}

	Tv vid;
	Pan lab;
	static int wid=1024;
	static int len=800;
	Timer t;

	Nabla(){
		setLayout(new GridLayout());
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		vid=new Tv(wid, len);
		add(vid,0,0);
		//lab=new Pan();
		//add(lab, 0,1);
		setSize(wid,len);
		setVisible(true);
		t = new Timer(1, new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				vid.m.update();
				vid.repaint();
			}

		});
		t.start();
		this.addKeyListener(this);
		vid.addMouseListener(this);
	}

	public void keyPressed(KeyEvent arg0) {
		if(arg0.getKeyCode()==84){
			vid.m.wave=!vid.m.wave;
		}else if(arg0.getKeyCode()==82){
			vid.m.reset();	
		}else if(arg0.getKeyCode()==80){
			if(t.isRunning()){
				t.stop();
				System.out.println("STOPED");
			}else{
				t.start();
				System.out.println("STARTED");
			}
		}else if(arg0.getKeyCode()==69){
			vid.m.fixE=!vid.m.fixE;	
		}else{
			System.out.println(arg0.getKeyCode());

		}
		repaint();
	}

	public void keyReleased(KeyEvent arg0) {}
	public void keyTyped(KeyEvent arg0) {}
	public void mouseClicked(MouseEvent arg0) {}
	public void mouseEntered(MouseEvent arg0) {}
	public void mouseExited(MouseEvent arg0) {}
	public void mousePressed(MouseEvent arg0) {}

	public void mouseReleased(MouseEvent arg0) {
		if(arg0.getButton()==3){
			vid.m.disturb(arg0.getX(),arg0.getY(), vid.res,-1);
		}else{
			vid.m.disturb(arg0.getX(),arg0.getY(), vid.res,1);
		}
		repaint();
	}
}
class Pan extends JPanel{

	private static final long serialVersionUID = 1L;

	public void paint(Graphics g){
		setSize(100,100);
		g.drawLine(0, 0, 100, 100);
	}
}

class Tv extends JPanel{

	private static final long serialVersionUID = 1L;
	Mod m;
	int res=5;
	Tv(int wid, int len){
		m=new Mod();
		this.setSize(new Dimension(wid,len));
	}

	public void paint(Graphics g){
		for(int i=0;i<m.wid;i++){
			for(int j=0;j<m.len;j++){
				double t=m.val[i][j];
				int col=Math.max(Math.min((int)(255*t),255),-255);

				g.setColor(new Color(Math.abs(col),Math.max(col,0),Math.max(col,0)));
				g.fillRect(res*i, res*j, res, res);
			}	
		}
	}

}

class Mod{
	boolean fixE=false;
	boolean wave=false;

	int wid=180;
	int len=140;
	double[][] val=new double[wid][len];
	double[][] dt=new double[wid][len];
	double[][] ddt=new double[wid][len];


	Mod(){
		reset();
	}

	void reset(){
		for(int i=0;i<wid;i++){
			for(int j=0;j<len;j++){
				val[i][j]=0;
				dt[i][j]=0;
				ddt[i][j]=0;

			}	
		}
	}
	void disturb(int x, int y, int res, double am){

		for(int i=0;i<wid;i++){
			for(int j=0;j<len;j++){
				if((i-x/res)*(i-x/res)+(j-y/res)*(j-y/res)<100){
					val[i][j]=val[i][j]+am;
				}
			}	
		}
	}

	double[][] ddx(double[][] in){
		double[][] out=new double[in.length-1][in[0].length];
		for(int i=0;i<in.length-1;i++){
			for(int j=0;j<in[0].length;j++){
				out[i][j]=in[i+1][j]-in[i][j];
			}	
		}
		return out;
	}	
	double[][] ddy(double[][] in){
		double[][] out=new double[in.length][in[0].length-1];
		for(int i=0;i<in.length;i++){
			for(int j=0;j<in[0].length-1;j++){
				out[i][j]=in[i][j+1]-in[i][j];
			}	
		}
		return out;
	}

	public void update() {

		double[][] dx=ddx(ddx(val));
		double[][] dy=ddy(ddy(val));

		for(int i=1;i<wid-1;i++){
			for(int j=1;j<len-1;j++){
				dt[i][j]=(dx[i-1][j] + dy[i][j-1]);

				if(!wave){
					val[i][j]=val[i][j]+dt[i][j]/5;
				}
				if(wave){
					ddt[i][j]=ddt[i][j]+dt[i][j]/5;
					val[i][j]=val[i][j]+ddt[i][j]+dt[i][j]/7;
				}	
			}
		}
		if(fixE){
			fixEdges();
		}else{
			fixEdges(0);
		}
	}
	void fixEdges(){
		for(int i=0;i<wid;i++){
			val[i][0]=val[i][1];
			val[i][val[0].length-1]=val[i][val[0].length-2];
		}
		for(int j=0;j<len;j++){
			val[0][j]=val[1][j];
			val[val.length-1][j]=val[val.length-2][j];
		}
	}
	void fixEdges(double am){
		for(int i=0;i<wid;i++){
			val[i][0]=val[i][1];
			val[i][val[0].length-1]=am;
		}
		for(int j=0;j<len;j++){
			val[0][j]=val[1][j];
			val[val.length-1][j]=am;
		}
	}
}
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import main.Cartographer;
import main.PopulationModel;

/**
 * 
 */

/**
 * This runs the PopulationModel for 4 given densities for single thread, and then 2,4,8,16 threads parallel. 
 * The output file "speedup.dat" gives the "density threads timeToComplete ans". The number of steps is 100
 * @author s0941897
 *
 */
@SuppressWarnings("unused")
public class PopulationModelRun {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new File("speedup.dat"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}


		PopulationModel p;

		//The number of steps and the tenPercent for printing the current progress
		int steps = 100;
		int tenPercent = steps/10;

		double[] testDensities = {0.2, 0.4, 0.6, 0.8};
		int[] testThreads = {3,2,4,8,16,32,64,100};	
	
		for(double den : testDensities){

			Cartographer c = Cartographer.generate(100, den, 5, 500.0, 3);

			//For the number of threads
			for(int n : testThreads){
				//Make a new model and set threads
				p = new PopulationModel(c.getMap(), c.getCivilisations(), null, null);
				p.setNumberOfThreads(n);

				//The all important start time
				long startTime = System.currentTimeMillis();

				System.out.println("\nSimulation Started on "+n+" threads. Land density: "+den);
				//If only one thread run the non parallel version of the iterate
				if(n==1){
					for(int t=0;t<steps;t++){
						p.iterateAll();
						progress(t,tenPercent);
					}
					//Else run the parallel one
				}else{
					for(int t=0;t<steps;t++){
						p.simulationStep();
						progress(t,tenPercent);
					}
				}
				//Print stuff out and flush it in case the world ends and i can look at some output in hell
				long endTime = System.currentTimeMillis();
				System.out.println("\nTotal Population: "+p.totalPopulation());
				System.out.println("Time to complete: "+(endTime-startTime));

				out.println(den+" "+n+" "+(endTime-startTime)+" "+p.totalPopulation());
				out.flush();
			}
			out.println();
		}
		//Close the Writer and stuff
		out.close();
		System.exit(0);
	}

	private static void progress(int t, int tenPercent){
		if(t%tenPercent == 0){
			System.out.print(">");
		}
	}
}



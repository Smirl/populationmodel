import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
 
/**
 * 
 */

/**
 * @author s0941897
 * 
 */
public class ShowMap extends JFrame implements MouseListener, MouseWheelListener,MouseMotionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	ArrayList<Point> points = new ArrayList<Point>();

	ArrayList<Point> map = new ArrayList<Point>();

	private static final Color[] colours = { new Color(255, 255, 224), new Color(255, 255, 0), new Color(255, 153, 103),
			new Color(255, 127, 0), Color.RED, new Color(159, 0, 197) };

	int xOffset = 2923;
	int yOffset = -3080;

	Point[] clicks = new Point[2];
	
	double scale = 0.5;

	JPanel pane = new JPanel() {
		/**
	 * 
	 */
		private static final long serialVersionUID = 1L;

		protected void paintComponent(java.awt.Graphics gg) {

			Graphics2D g = (Graphics2D) gg;

			// g.scale(0.1, 0.1);

			

			g.setColor(Color.BLUE);
			g.fillRect(0, 0, getWidth(), getHeight());

			g.scale(scale,scale);

			for (int i = 0; i < map.size(); i++) {
				g.setColor(Color.GREEN);
				g.fillRect(map.get(i).x - xOffset, getHeight() - map.get(i).y - yOffset, 1, 1);
			}

			for (int i = 0; i < points.size(); i++) {

				points.get(i).draw(g);
				g.fillRect(points.get(i).x - xOffset, getHeight() - points.get(i).y - yOffset, 1, 1);
				
				if (i >= 1946461) {
					g.setColor(Color.YELLOW);
					drawCircle(g, points.get(i).x - xOffset, getHeight() - points.get(i).y - yOffset, 5);
				}
			}

			for (int i = 0; i < map.size(); i++) {
				if (map.get(i).x % 100 == 0 && map.get(i).y % 100 == 0) {
					g.setColor(Color.WHITE);
					g.drawString(points.get(i).x + " " + points.get(i).y, points.get(i).x - xOffset, getHeight() - points.get(i).y - yOffset);
				}
			}

			// System.out.println("Repainted");

		}
	};

	/**
	 * @throws HeadlessException
	 */
	public ShowMap() throws HeadlessException {
		setDefaultLookAndFeelDecorated(true);
		setPreferredSize(new Dimension(1000, 1000));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		try {
			readFile();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		pane.setPreferredSize(new Dimension(1000, 1000));

		add(pane);

		pack();

		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addMouseWheelListener(this);
		
		clicks[0] = new Point(0, 0, 0);
		clicks[1] = new Point(0, 0, 0);

		//	setDefaultCloseAction(JFrame.EXIT_ON_CLOSE);

		setVisible(true);
	}

	public void readFile() throws FileNotFoundException, NumberFormatException, IOException {

		BufferedReader br = new BufferedReader(new FileReader(new File("../popEurope.dat")));

		BufferedReader mapReader = new BufferedReader(new FileReader(new File("../landEurope.dat")));

		String line;
		String[] parts;
		int counter = 0;

		while ((line = br.readLine()) != null) {
			parts = line.split(" ");
			points.add(new Point(Integer.parseInt(parts[1]), Integer.parseInt(parts[0]), Integer.parseInt(parts[2])));
			counter++;
		}
		System.out.println("POP DATA: File Read " + counter + " lines");
		counter=0;
		while ((line = mapReader.readLine()) != null) {
			parts = line.split(" ");
			map.add(new Point(Integer.parseInt(parts[1]), Integer.parseInt(parts[0]), 0));
			counter++;
		}

		System.out.println("MAP DATA: File Read " + counter + " lines");

	}

	public static void main(String[] args) {
		new ShowMap();
	}

	protected AlphaComposite makeComposite(float alpha) {
		int type = AlphaComposite.SRC_OVER;
		return (AlphaComposite.getInstance(type, alpha));
	}

	public void drawCircle(Graphics2D g, int xCenter, int yCenter, int r) {

		g.fillOval(xCenter - r, yCenter - r, 2 * r, 2 * r);

	}

	public class Point {
		int x;
		int y;
		int val;

		public Point(int i, int j, int v) {
			x = i;
			y = j;
			val = v;
		}

		public void draw(Graphics2D g) {
			if (val <= 0) {
				return;
			}

			int i = 0;
			if (val <= 4) {
				i = 0;
			} else if (val > 4 && val <= 19) {
				i = 1;
			} else if (val > 19 && val <= 199) {
				i = 2;
			} else if (val > 199 && val <= 499) {
				i = 3;
			} else if (val > 499 && val <= 5000) {
				i = 4;
			} else {
				i = 5;
			}

			g.setColor(colours[i]);
			//g.fillRect(x - xOffset, getHeight() - y - yOffset, 1, 1);
		}
	}

	//MouseListener
	@Override
	public void mouseClicked(MouseEvent e) {
		points.add(new Point(e.getX() + xOffset, getHeight() - yOffset - e.getY(), 0));
		System.out.println("Mouse Clicked" + (e.getX() + xOffset) + " " + (getHeight() - yOffset - e.getY()));

		pane.repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		clicks[0] = new Point(e.getX(), e.getY(), 0);

	}

	@Override
	public void mouseReleased(MouseEvent e) {


	}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}

	//MouseWheelListener
	
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		scale += e.getWheelRotation()*0.1;
		if(scale < 0.1) scale=0.1;
		repaint();
	}

	//MouseMotionListener
	
	@Override
	public void mouseDragged(MouseEvent e) {
		clicks[1] = new Point(e.getX(), e.getY(), 0);

		xOffset += clicks[1].x - clicks[0].x;
		yOffset += clicks[1].y - clicks[0].y;

		pane.repaint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {}

}


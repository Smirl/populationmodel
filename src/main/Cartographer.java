package main;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.QuadCurve2D;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JPanel;

import main.data.Civilisation;
import main.data.Island;
import main.data.Site;

import viewer.GridViewer;

/**
 * A map creator for the PopulationModel. <br> <br>
 *  - Runs in about 297148 milliseconds when unthreaded <br>
 *  - Runs in about 314239 milliseconds when using Executor 4 threads <br>
 *  - Runs in about 332012 milliseconds when using Executor 10 threads  <br>
 *  - Runs in about 240223 milliseconds when using Executor 4 threads with invokeAll() DualCore<br>
 *  - Runs in about 363816 milliseconds when using Executor 4 threads with invokeAll() QuadCore p.s. I give up <br>
 *  - Runs in about 82795 milliseconds when using Executor 10 threads with invokeAll() (on dual core mac)<br>
 * @author Alex
 *
 */
public class Cartographer{

	//*********** FIELDS ***********
	/**
	 * The random number generator
	 */
	public static Random r = new Random();

	//The offsets
	protected int xOffset = GridViewer.xOffset;
	protected int yOffset = -GridViewer.yOffset;

	//The Cartographer settings
	private int numberOfIslands;
	private static double longestSide;
	private double defaultk = 100;

	//Drawing parameters for the viewer
	private double scale = 1.0;
	private double drawScale = 100.0;
	private Point[] clicks = new Point[2];
	private int paintMode = 0;

	//Storage of sites
	protected HashMap<String, Site> cartWorkingMap = new HashMap<String, Site>();
	protected ArrayList<Island> cartIslands = new ArrayList<Island>();
	protected ArrayList<Civilisation> cartCivilisations = new ArrayList<Civilisation>();

	//temp inner storage of sites
	private ArrayList<Site> withNeighbours= new ArrayList<Site>();
	private ArrayList<Site> islandSeedSites= new ArrayList<Site>();
	private ArrayList<Arc> links = new ArrayList<Arc>();

	//*********** ANNON INNER TYPES ***********

	private MouseWheelListener wheelListener;
	private MouseListener mouseListener;
	private KeyListener keyListener;
	public JPanel pane;

	//*********** CONTRUCTORS ***********
	/**
	 * Make a cartogpraher. (call a static method for public use)
	 */
	public Cartographer(){
		r = new Random();
	}

	private Cartographer(HashMap<String,Site> map){
		cartWorkingMap = map;
	}

	private void initilise(int seed){
		if(seed < 0){
			r = new Random();
		}else{
			r = new Random(seed);
		}
	}


	//*********** PUBLIC STATIC METHODS ***********
	/**
	 * Makes a cartographer and completely generates land from the given arguements. This returns the Cartograher object which contains The landsites with
	 * neighbours and a list of islands that they belong to. 
	 * @param seed The seed to create and island with. If this is negative a random seed will be choosen.
	 * @param landDensity The approx. land density of the region to be generated such that the number of sites is L*L*landDensity
	 * @param approxIslands The aprrox. number of islands that the map should have. They can stick together when being created.
	 * @param longestSide Otherwise known as L. The lenght of the square the map is generated in. Some sites may lie outside region. 
	 */
	public static Cartographer generateIslands(int seed, double landDensity, int approxIslands, double longestSide, int civs){
		Cartographer cartographer = new Cartographer();

		cartographer.initilise(seed);

		cartographer.generateLand(landDensity, approxIslands, longestSide);
		cartographer.findNeighbours(true);
		cartographer.findIslands();
		cartographer.addCarryingCapacity(false);
		cartographer.addPeople(civs);
		cartographer.addPorts();
		
		return cartographer;
	}
	
	public static Cartographer generateSquare(int seed, double longestSide, int civs, boolean flatk){
		Cartographer cartographer = new Cartographer();
		
		cartographer.initilise(seed);
		
		cartographer.generateSquareLand(longestSide);
		cartographer.findNeighbours(true);
		cartographer.findIslands();
		cartographer.addCarryingCapacity(flatk);
		cartographer.addPeople(civs);
		
		return cartographer;
	}
	
	public static Cartographer generateArena(int seed, int size, boolean flatk){
		Cartographer cartographer = new Cartographer();
		cartographer.initilise(seed);
		
		cartographer.generateSquareLand(size);
		cartographer.findNeighbours(true);
		cartographer.findIslands();
		cartographer.addCarryingCapacity(flatk);
		cartographer.addCornerCivs(size);
		
		return cartographer;
	}
	
	public static Cartographer generateCenter(int seed, int size, boolean flatk){
		Cartographer cartographer = new Cartographer();
		cartographer.initilise(seed);
		
		cartographer.generateSquareLand(size);
		cartographer.findNeighbours(true);
		cartographer.findIslands();
		cartographer.addCarryingCapacity(flatk);
		cartographer.addCenterCivs(size);
		
		return cartographer;
	}

	public static Site[] findNeighbours(Site[] map){
		HashMap<String, Site> tempMap = new HashMap<String, Site>();
		for(Site s : map){
			tempMap.put(s.locationString(), s);
		}
		new Cartographer(tempMap).findNeighbours(false);
		return map;
	}
	

	//*********** PUBLIC METHODS ***********

	/**
	 * Draw a NEW frame showing the output at it's current state. <br>
	 * This will repaint if zoomed/moved so if called more than once do not repaint it.
	 */
	public void draw(){

		setUpViewer();

		pane.setPreferredSize(new Dimension(500,500));
		pane.addMouseWheelListener(wheelListener);
		pane.addMouseListener(mouseListener);
		pane.addKeyListener(keyListener);
		pane.setFocusable(true);

		JFrame f = new JFrame();
		int width = (int)longestSide;
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setPreferredSize(new Dimension(width, width));

		f.add(pane);

		f.pack();

		f.setVisible(true);
	}

	/**
	 * Convert the interal workingMap into a plain text file
	 * @param mapData the File to store it in
	 */
	public void mapToFile(File mapData){
		
		Site[] outputMap = new Site[cartWorkingMap.values().size()];
		
		//To avoid the stack overflow error
		for(Site site : cartWorkingMap.values()){
			site.setNeighbours(null);
		}
		
		int counter = 0;
		for(Site s : cartWorkingMap.values()){
			outputMap[counter] = s;
			counter++;
		}
		
		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(new FileOutputStream(mapData));
			out.writeObject(outputMap);
			out.close();
		} catch (Exception e) {
			return;
		}
		
		//Add the neighbours again
		findNeighbours(false);
	}

	/**
	 * Convert the HashMap<String,Site> workingMap into Site[]
	 * @return the usable Site[] of the workingMap
	 */
	public Site[] getMap(){
		Site[] output = new Site[cartWorkingMap.size()];

		int counter = 0;
		for(Site s : cartWorkingMap.values()){
			output[counter] = s;
			counter++;
		}

		return output;
	}

	/**
	 * Get the island array
	 * @return
	 */
	public ArrayList<Island> getIslands(){
		return cartIslands;
	}

	/**
	 * Get the civilisations
	 * @return
	 */
	public ArrayList<Civilisation> getCivilisations(){
		return cartCivilisations;
	}
	
	//*********** Protected Methods ************
	/**
	 * DOES NOTHING BY DEFAULT<br>
	 * Override this method with other custom methods to generate land, etc. from outside annomous inner classes
	 */
	public void generate(){}
	
	/**
	 * Set all carrying capacities (K & K0) to 0.0
	 */
	protected void clearK(double val){
		for (Site site : cartWorkingMap.values()) {
			site.setK(val);
			site.setK0(val);
		}
	}
	
	/**
	 * Clear all land from the map
	 */
	protected void clearLand(){
		cartWorkingMap.clear();
	}
	
	/**
	 * Remove all civilisations and all densities from each site
	 */
	protected void clearCivilisations(){
		cartCivilisations.clear();
		for(Site site : cartWorkingMap.values())
			site.getPopulation().clear();
	}
	

	//*********** PRIVATE HELPER METHODS ***********
	/**
	 * Generate a map using the "Stick to the coast" method. This is quite slow. 
	 * @see circleGenerator
	 * @param landDensity The fraction of the area to cover in land
	 * @param approxIslands Try to create this many island. They can stick together
	 * @return A Site array of the points
	 */
	private void generateLand(double landDensity, int approxIslands, double longSide){

		longestSide = longSide;
		numberOfIslands = approxIslands;

		//The area to create sites
		double minX = 0.0;
		double maxX = longestSide;

		//A temp site for testing locations, etc.
		Site tempSite = new Site();
		Site putSite = new Site();

		//Used to loop until a neighbour has 500, 500been found (if one exists)
		boolean localSiteFound = false;

		//The offset from a site to look for a neighbour
		int[] offset = {0,0};

		//Total number of site to create
		int numberOfSites = (int) (longestSide*longestSide*landDensity);
		System.out.println(numberOfSites + " to be created on approximately " + approxIslands + " islands");

		//Used for a progress bar
		int fivePercent = numberOfSites/20;

		//Make the island starting sites
		int i = 0;
		while(i < approxIslands){
			tempSite.setLocation(xOffset + (int)randomDouble(minX, maxX), yOffset + (int)randomDouble(minX, maxX));
			if(contains(tempSite)){
				continue;
			}
			putSite = tempSite.copyWithoutNeighbour();
			workingMapPut(putSite);
			withNeighbours.add(putSite);
			islandSeedSites.add(putSite);

			i++;
		}

		//Add to the island to create map
		while(i < numberOfSites){

			localSiteFound = false;

			//See if nearby site exists
			Site testSite = randomSiteWithNeighbour();
			if(hasFreeNeighbour(testSite)){
				while(!localSiteFound){
					offset = randomDirection();
					tempSite.setLocation(testSite.getX() + offset[0], testSite.getY() + offset[1]);
					if(!contains(tempSite)){
						localSiteFound = true;
					}
				}
			}else{
				synchronized (withNeighbours) {
					withNeighbours.remove(testSite);
				}
				continue;
			}

			putSite = tempSite.copyWithoutNeighbour();
			cartWorkingMap.put(putSite.locationString(), putSite);
			withNeighbours.add(putSite);
			i++;

			if(i % fivePercent == 0 ){
				System.out.print(">");
			}

			synchronized (withNeighbours) {
				try {
					pane.repaint();
				} catch (Exception e) {
				}
			}
		}

		System.out.println(" LAND GENERATED");
		System.gc();

	}

	private void generateSquareLand(double longSide){
		int fivePercent = (int)longSide/20;
		numberOfIslands=1;
		
		//Add the start site
		Site origin = new Site(xOffset,yOffset);
		workingMapPut(origin);
		islandSeedSites.add(origin);
		
		//Add the other sites
		for (int i = 0; i < (int)longSide; i++) {
			for (int j = 0; j< (int)longSide; j++) {
				if(i==0&&j==0)
					continue;
				workingMapPut(new Site(xOffset + i,yOffset + j));
			}
			
			if(i%fivePercent==0)
				System.out.print(">");
		}
		
		System.out.println(" LAND GENERATED");
		System.gc();
	}
	
	protected void findNeighbours(boolean output){
		String[] splitKey;
		int[] intKey = new int[2];
		ArrayList<Site> neebors = new ArrayList<Site>();

		int fivePercent = cartWorkingMap.size()/20;
		int counter = 0;
		Site putSite = null;

		for(String key : cartWorkingMap.keySet()){
			//Split the string into x and y strings
			splitKey = key.split(" ");

			//Parse these into ints
			intKey[0] = Integer.parseInt(splitKey[0]);
			intKey[1] = Integer.parseInt(splitKey[1]);


			if((putSite = cartWorkingMap.get((intKey[0]-1)+" "+ intKey[1])   ) != null) neebors.add(putSite);
			if((putSite = cartWorkingMap.get((intKey[0]+1)+" "+ intKey[1])   ) != null) neebors.add(putSite);
			if((putSite = cartWorkingMap.get( intKey[0]   +" "+(intKey[1]-1))) != null) neebors.add(putSite);
			if((putSite = cartWorkingMap.get( intKey[0]   +" "+(intKey[1]+1))) != null) neebors.add(putSite);

			cartWorkingMap.get(key).setNeighbours((Site[]) neebors.toArray(new Site[neebors.size()]));
			neebors.clear();

			if(output && counter%fivePercent ==0){
				System.out.print(">");
			}
			counter++;
		}

		neebors=null;

		if(output) System.out.println(" NEIGHBOURS FOUND");
	}



	/**
	 * Find the Islands (collection of Sites touching one another) 
	 * @return Island array
	 * @see Island
	 */
	private void findIslands(){
		int tics =  20/numberOfIslands;
		if(tics  < 1)tics = 1;

		int counter = 0;
		for(Site seedSite : islandSeedSites){
			cartIslands.add(new Island(seedSite, counter));
			cartIslands.get(counter).explore();

			for(int t=0; t<tics; t++){
				System.out.print(">");
			}

			counter++;
		}

		//Remove islands which have connected together. Also set the internal toExplore arraylist to null to save space

		for(int i =0; i < cartIslands.size(); i++){
			for(int j =0; j < cartIslands.size(); j++){
				if(cartIslands.get(i).sameIsland(cartIslands.get(j)) && cartIslands.get(i).ID!=cartIslands.get(j).ID){
					cartIslands.remove(cartIslands.get(i));
					if(i>0) i--;
					break;
				}
			}
			//Set the internal toExplore list to null
			cartIslands.get(i).toExplore = null;
		}		


		System.out.println(" ISLANDS FOUND");
	}

	private void addCarryingCapacity(boolean flatk){
		
		//If setting a flat carrying capacity
		if(flatk){
			for (Site site : cartWorkingMap.values()) {
				site.setK(defaultk);
				site.setK0(defaultk);
			}
			return;
		}

		//Randomly fill carringCapacity
		//More around the coast
		for(Site site : withNeighbours){
			if(randomDouble(0.0, 1.0) < 0.2){
				site.setK(randomDouble(0.0, 1000.0));
			}
		}
		//Some inland
		for (Site site : cartWorkingMap.values()) {
			if(randomDouble(0.0, 1.0) < 0.05){
				site.setK(randomDouble(0.0, 1000.0));
			}
		}

		//Diffuse this a little
		double constant = 0.5*0.01;
		double nn;
		int steps = 400;
		int fivePercent = steps/20;

		for (int i = 0; i < steps; i++) {
			for(Site site : cartWorkingMap.values()){
				//reset the nn contribution
				nn = 0.0;
				//Sum the neighbour contributions
				for (int j = 0; j < site.getNeighbours().length; j++) {
					nn += site.getNeighbour(j).getK();
				}
				nn -= site.getNeighbours().length  * site.getK();

				//Update the K
				site.setK(site.getK() + (constant*nn));
			}

			if(i% fivePercent ==0){
				System.out.print(">");
			}
		}
		
		//set the natural k
		for(Site site : cartWorkingMap.values())
			site.setK0(site.getK());

		System.out.println(" ADDED CARRYING CAPACITY");
	}

	private void addPeople(int civs){

		int numberOfCivs = civs;
		int tics =  20/numberOfCivs;
		if(tics  < 1)tics = 1;
		
		//Make sure that all sites have an entry for every population. Save's on try/catch and nullpointerExceptions
		for(Site site : cartWorkingMap.values()){
			site.addPopulation(numberOfCivs-1, 0.0);
		}

		for (int i = 0; i < numberOfCivs; i++) {
			cartCivilisations.add(new Civilisation());
			int inhabited = 0;
			for(Island is : cartIslands){
				if(randomDouble(0.0, 1.0) < (1.0/(double)numberOfCivs)){
					is.randomAddCivilisation(i,numberOfCivs);
					inhabited++;
				}
			}
			
			if(inhabited==0)
				cartIslands.get(r.nextInt(cartIslands.size())).randomAddCivilisation(i, numberOfCivs);

			for(int t=0; t<tics; t++){
				System.out.print(">");
			}
		}

		//Check all islands have people
		for(Island is : cartIslands){
			if(is.totalPopulation() <= 0.0){
				int civToAdd = r.nextInt(cartCivilisations.size());
				is.randomAddCivilisation(civToAdd,numberOfCivs);
			}
		}

		System.out.println(" PEOPLE ADDED in "+numberOfCivs+" CIVS");

	}
	
	/**
	 * 
	 * @param size
	 */
	private void addCornerCivs(int size){
		//civilisations.add(new Civilisation());
		//civilisations.add(new Civilisation());
		
		cartCivilisations.add(new Civilisation(0.5, 0.0, 0.5));
		cartCivilisations.add(new Civilisation(0.5, 0.5, 0.0));
		
		Site a = cartWorkingMap.get(xOffset+" "+(yOffset+size - 1));
		double kk = a.getK();
		a.addPopulation(0, a.getK());
		
		a = cartWorkingMap.get((xOffset + size - 1)+" "+yOffset);
		a.addPopulation(1, a.getK());
		System.out.println("   "+kk +" " + a.getK());
		
		//Make sure that every site has the civilisations on it
		for(Site site : cartWorkingMap.values()){
			site.addPopulation(1, 0.0);
		}
	}
	
	private void addCenterCivs(int size){
		cartCivilisations.add(new Civilisation());
		
		Site a = cartWorkingMap.get((xOffset+(size/2))+" "+(yOffset+(size/2)));
		a.addPopulation(0, a.getK());
		
		//Make sure that every site has the civilisations on it
		for(Site site : cartWorkingMap.values()){
			site.addPopulation(0, 0.0);
		}
	}

	/**
	 * Randomly add extra neighbours to some sites to act as ports
	 */
	private void addPorts(){
		links.clear();

		ArrayList<Site> ports = new ArrayList<Site>();

		int numberOfPorts = r.nextInt(numberOfIslands*3)+2;
		int tics =  20/numberOfPorts;
		if(tics  < 1)tics = 1;

		for (int i = 0; i < numberOfPorts; i++) {
			ports.add(withNeighbours.get(r.nextInt(withNeighbours.size())));
		}

		int connections;
		Site end; 
		for(Site site : ports){
			connections = r.nextInt(5);

			for (int i = 0; i < connections; i++) {
				end = ports.get(r.nextInt(ports.size()));
				if(end.equals(site)){
					i--;
					continue;
				}
				links.add(new Arc(site.getX(), site.getY(), end.getX(), end.getY()));

			}

			for(int t=0; t<tics; t++){
				System.out.print(">");
			}
		}
		System.out.println(" PORTS PLACED");
	}


	private void workingMapPut(Site putSite){
		synchronized (cartWorkingMap) {
			cartWorkingMap.put(putSite.locationString(), putSite);
		}
	}


	private void clean(){
		cartIslands.clear();
		islandSeedSites.clear();
		withNeighbours.clear();
		cartWorkingMap.clear();
	}

	/**
	 * Get a random site from the "coast" 
	 * @return A site which probably has a neighbour site. Best practise to remove from list if it doesnt
	 */
	private Site randomSiteWithNeighbour(){
		return withNeighbours.get(r.nextInt(withNeighbours.size()));
	}

	/**
	 * Get a random coordinate of a neighbour site
	 * @return
	 */
	private int[] randomDirection(){
		int selection = r.nextInt(4);
		if(selection==0)
			return new int[]{-1, 0};
		if(selection==1)
			return new int[]{1, 0};
		if(selection==2)
			return new int[]{0, 1};
		if(selection==3)
			return new int[]{0, -1};

		return null;
	}

	/**
	 * See if a site has any neighbours in the working map. Multithreaded for speed
	 * @param a the site to test
	 * @return True if >1 neighbour site is not already created
	 */
	private boolean hasFreeNeighbour(Site a){
		boolean[] hasFreeNeighbour = {false,false,false,false};
		Site tempSite = new Site();

		tempSite.setLocation(a.getX() - 1, a.getY());
		hasFreeNeighbour[0] = !contains(tempSite);

		tempSite.setLocation(a.getX() + 1, a.getY());
		hasFreeNeighbour[1] = !contains(tempSite);

		tempSite.setLocation(a.getX(), a.getY() - 1);
		hasFreeNeighbour[2] = !contains(tempSite);

		tempSite.setLocation(a.getX(), a.getY() + 1);
		hasFreeNeighbour[3] = !contains(tempSite);

		return (hasFreeNeighbour[0] || hasFreeNeighbour[1] || hasFreeNeighbour[2] || hasFreeNeighbour[3]);
	}

	/**
	 * See if the workingMap contains a site at location of a
	 * @param a the test location
	 * @return True if workingMap contains a site at location of a
	 */
	private boolean contains(Site a){
		return cartWorkingMap.containsKey(a.locationString());
	}

	/**
	 * Create a random double between min and max
	 */
	public static double randomDouble(double min, double max){
		return r.nextDouble()*(max-min) + min;
	}

	private void setUpViewer(){
		wheelListener = new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent arg0) {
				if(arg0.isControlDown()){
					drawScale += 20.0*arg0.getWheelRotation();
					if(scale<0.0) scale = 0.0;
				}else{
					scale += 0.1*arg0.getWheelRotation();
					if(scale>5.0) scale = 5.0;
					if(scale<0.0) scale = 0.0;
				}
				pane.repaint();			
			}
		};

		mouseListener = new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				clicks[1] = new Point(arg0.getX(), arg0.getY());

				xOffset -= clicks[1].x - clicks[0].x;
				yOffset += clicks[1].y - clicks[0].y;

				pane.repaint();
			}
			@Override
			public void mousePressed(MouseEvent arg0) {
				clicks[0] = new Point(arg0.getX(),arg0.getY());
			}
			//unused
			@Override
			public void mouseClicked(MouseEvent arg0) {}
			@Override
			public void mouseExited(MouseEvent arg0) {}
			@Override
			public void mouseEntered(MouseEvent arg0) {}
		};

		keyListener = new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {}
			@Override
			public void keyReleased(KeyEvent e) {}

			@Override
			public void keyPressed(KeyEvent e) {
				//New Map
				if(e.getKeyCode() == KeyEvent.VK_N){
					clean();
					long newSeed = r.nextLong();
					r = new Random(newSeed);
					double den = randomDouble(0.0, 1.0);
					int isNo = r.nextInt(20)+1;
					System.out.println("Land Density: "+den+" Islands: "+isNo);
					generateLand(den, isNo, longestSide);
					findNeighbours(true);
					findIslands();
					addCarryingCapacity(false);
					addPeople(3);
					addPorts();
				}
				//Change view
				if(e.getKeyCode() == KeyEvent.VK_0){
					paintMode = 0;
				}
				if(e.getKeyCode() == KeyEvent.VK_1){
					paintMode = 1;
				}
				if(e.getKeyCode() == KeyEvent.VK_2){
					paintMode = 2;
				}
				//Add K
				if(e.getKeyCode() == KeyEvent.VK_K){
					addCarryingCapacity(false);
				}
				if(e.getKeyCode() == KeyEvent.VK_P){
					addPorts();
				}
				pane.repaint();
			}
		};

		pane = new JPanel(){
			private static final long serialVersionUID = 1L;

			/**
			 * Makes an AlphaComposite with an opacity of <code>alpha</code>
			 * @param alpha
			 * @return The new AlphaComposite
			 */
			protected AlphaComposite makeComposite(float alpha) {
				int type = AlphaComposite.SRC_OVER;
				return(AlphaComposite.getInstance(type, alpha));
			}

			protected void paintComponent(java.awt.Graphics gg) {

				Graphics2D g = (Graphics2D)gg;

				g.setColor(Color.BLUE);
				g.fillRect(0, 0, getWidth(), getHeight());
				g.setColor(Color.WHITE);
				String scaleString = String.format("%.1f", scale);
				String drawScaleString = String.format("%.1f", drawScale);
				g.drawString("ZOOM SCALE: "+scaleString+"DRAW SCALE: "+drawScaleString+" xOffset:"+xOffset+" yOffset: "+yOffset, 10, 10);

				g.scale(scale, scale);

				//BASE LAND
				g.setComposite(makeComposite(1.0f));
				for (Site site : cartWorkingMap.values()) {
					try {
						g.setColor(Color.WHITE);
						site.draw(g, xOffset, -yOffset, getHeight());
					} catch (Exception e) {
						System.out.println("I'm not here");
					}
				}

				g.setColor(Color.WHITE);
				for(Arc a : links){
					a.draw(g, xOffset, -yOffset, getHeight());
				}

				if(paintMode == 0){
					g.setComposite(makeComposite(1.0f));
					for(Island is : cartIslands){
						try{
							is.draw(g, xOffset, yOffset, getHeight());
						}catch(Exception arg0){
							arg0.printStackTrace();
						}
					}
					g.setColor(Color.WHITE);

					for(Island is : cartIslands){
						try{
							g.drawString(is.ID+"", is.startSite.getX() - xOffset, getHeight() - is.startSite.getY() + yOffset);
						}catch(Exception arg0){
							arg0.printStackTrace();
						}
					}
				}else if(paintMode ==1){
					g.setColor(Color.BLACK);
					float alpha;
					for(Site site : cartWorkingMap.values()){
						try{
							alpha = (float)(site.getK()/drawScale);
							g.setComposite(makeComposite(alpha));
						}catch(Exception arg0){
							g.setComposite(makeComposite(1.0f));
						}
						site.draw(g, xOffset, -yOffset, getHeight());
					}
				}else if(paintMode == 2){
					float alpha;
					for(Site site : cartWorkingMap.values()){
						for(int i = 0; i<cartCivilisations.size(); i++){
							try{
								alpha = (float)(site.getPopulation(i)/drawScale);
								g.setComposite(makeComposite(alpha));
							}catch(Exception arg0){
								g.setColor(Color.GRAY);
								g.setComposite(makeComposite(0.2f));
							}
							g.setColor(Island.colours[i%Island.colours.length]);
							site.draw(g, xOffset, -yOffset, getHeight());
						}
					}
				}
			}
		};
	}


	//*********** TEST MAIN ***********
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		//Start time
		long startTime = System.currentTimeMillis();

		//***** MAKE LAND **********

		//Cartographer c = Cartographer.generate(100, 0.2, 5, 500.0, 3);
		//Cartographer c = Cartographer.generateSquare(100, 100, 3);
		Cartographer c = Cartographer.generateIslands(100, 0.1, 10, 500.0, 3);
		c.mapToFile(new File("1000-2-5-500.map"));

		//End time
		long executionTime = System.currentTimeMillis() - startTime;
		System.out.println((float)executionTime/1000.0f+" secs to complete \n");

		c.draw();
		for(Island i : c.getIslands()){
			System.out.println("  Island "+i.ID+": "+i.landMass.size());
		}
		/*
		//c.createPorts(15);
		c.draw();
		//***** FIND ISLANDS **********
		ArrayList<Island> islands = c.getIslands();

		int total = 0;
		int islandNumber =0;
		for(Island i : islands){
			System.out.println("  Island "+islandNumber+": "+i.landMass.size());
			total += i.landMass.size();
			islandNumber++;
		}
		System.out.println("  Total: "+total);
		*/
	}

	private class Arc {
		int x1,x2,y1,y2;

		public Arc(int x, int y, int xx, int yy){
			this.x1 = x;
			this.y1 = y;
			this.x2 = xx;
			this.y2 = yy;
		}

		public void draw(Graphics2D g, int xOffset, int yOffset, int h){
			double dx = (double)(x2-x1)*2.0;
			double dy = (double)(y2-y1)*2.0;
			QuadCurve2D q = new QuadCurve2D.Double();
			//g.drawLine(x1-xOffset,h - yOffset - y1,x2 - xOffset ,h - yOffset - y2);
			q.setCurve((double)x1 - xOffset, h - yOffset - (double)y1, (double)x1+dx - xOffset, h - yOffset - (double)y1+dy, (double)x2 - xOffset, h - yOffset - (double)y2);
			g.draw(q);
		}

	}






}
/**
 * 
 */
package main;

import java.util.Random;

/**
 * An object to represent a civilisation. Stores growth rates, name and average population density
 * 
 * @author s0941897
 *
 */
public class Civilisation {
	
	//Static variables
	private static int nextIndex = 0;
	
	private static String[] names = {"Barbarians","Mayans","Romans","Britons"};

	//Instance Variables
	private String name;

	private int index;

	//The EGT relevant variables
	private double growthRate;
	private double intellect;
	private double aggression;

	//Data concerning the civilisation
	private double averageDensity;
	
	private double totalPop;

	/**
	 * Create a civilisation when the model is known
	 */
	public Civilisation(String name, int index, double growth, double intellect, double aggression) {
		this.name = name;
		this.index = index;
		
		this.growthRate = growth;
		this.intellect = intellect;
		this.aggression = aggression;
		
		this.totalPop = 0.0;

		nextIndex++;
	}
	
	/**
	 * Create a civilisation when the model is not known or unchanged and a name is not important.
	 */
	public Civilisation(int index, double growth, double intellect, double aggression){
		this(randomName(index),index, growth, intellect, aggression);
	}
	
	/**
	 * Create a new Civilisation with the set strategy
	 * @param growth
	 * @param intellect
	 * @param aggression
	 */
	public Civilisation(double growth,double intellect, double aggression){
		this(randomName(nextIndex), nextIndex, growth, intellect, aggression);
	}
	
	/**
	 * Create a random Civilisation with
	 */
	public Civilisation(){
		this(randomName(nextIndex),nextIndex,0.0,0.0,0.0);
		growthRate = Cartographer.r.nextDouble();
		intellect  = Cartographer.r.nextDouble() * (1.0 - growthRate);
		aggression = 1.0 - growthRate - intellect;
	}

	//********** Getters and Setters **********************
	//Getters
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public int getIndex(){
		return index;
	}

	/**
	 * @return the growthRate
	 */
	public double getGrowthRate() {
		return growthRate;
	}
	/**
	 * @return the intellect
	 */
	public double getIntellect() {
		return intellect;
	}
	/**
	 * @return the aggression
	 */
	public double getAggression() {
		return aggression;
	}

	/**
	 * @return the averageDensity
	 */
	public double getAverageDensity() {
		return averageDensity;
	}
	
	/**
	 * @return the total population
	 */
	public double getTotalPopulation(){
		return totalPop;
	}
	//Setters
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param growthRate the growthRate to set
	 */
	public void setGrowthRate(double growthRate) {
		this.growthRate = growthRate;
	}

	/**
	 * @param averageDensity the averageDensity to set
	 */
	public void setAverageDensity(double averageDensity) {
		this.averageDensity = averageDensity;
	}

	//************ Methods ********************************

	public void calculateAverageDensity(Site[] map){
		averageDensity = 0.0;
		double numLandAreas = 0;

		for(int i=0; i<map.length; i++){
			averageDensity += map[i].getPopulation(index);
			numLandAreas += 1.0;
		}
		averageDensity = averageDensity/numLandAreas;
	}
	
	public double calculateTotalPopulation(){
		if(Model.model==null)
			return -1.0;
		totalPop = 0.0;
		for (int i = 0; i < Model.model.getMap().length; i++) {
			totalPop += Model.model.getMap()[i].getPopulation(index);
		}
		return totalPop;
	}

	//************ Static Methods ********************************
	/**
	 * Return the next available index based on the current number of civilisation in the model
	 */
	public static int nextAvailableIndex(){
		return nextIndex;
	}

	/**
	 * Calculate a random name of a civilisation with the index included.
	 * @param index The index of the civilisation
	 * @return A random String appended with the index given. 
	 */
	public static String randomName(int index){
		return names[(new Random().nextInt(names.length))] + index;
	}
	
	/**
	 * Set the model that all civilisations live in.
	 */
	public static void setModel(Model m){
		Model.model = m;
	}
	
	/**
	 * Set the next available index to a new value. Call reset to return it to zero
	 * @param i the next unique index
	 */
	public static void setNextIndex(int i){
		nextIndex = i;
	}

	/**
	 * Reset the starting index if a new model is created
	 */
	public static void reset(){
		nextIndex = 0;
	}
	
	@Override
	public String toString() {
		return name + "\t r:" + growthRate + "\t I:" + intellect +"\t A:"+ aggression;
	}

}

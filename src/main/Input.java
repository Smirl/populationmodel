package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import main.data.Civilisation;
import main.data.Parameters;
import main.data.Site;

/**
 * Reads in the map from the input file and generates a double array of Sites of appropriate size to pass to the Model
 * 
 * @author Evguenia Usoskina
 *
 */
public class Input {

	private static double defaultK = 100.0;

	// ~~~~~~~~~~ Static Methods ~~~~~~~~~~
	/**
	 * Reads the map file and generates and returns a corresponding array of sites with land information<br>
	 * @param landFileLocation The location and name of the file to load without .land or .pop extentions. i.e. "../UK"
	 * @return The site array with corresponding site data
	 */
	public static Site[] loadMapData(String landFileLocation) throws IOException{

		Site[] grid;

		File landFile = new File(landFileLocation);

		if(!landFile.exists()){
			throw new FileNotFoundException(landFileLocation+" does not exist");
		}

		//Read from the data file (it is not null)
		BufferedReader br = new BufferedReader (new FileReader(landFile));

		//Get the number of points
		int counter = 0;
		while(br.readLine() != null){
			counter++;
		}
		br.close();

		System.out.println("File has "+counter+" points. Begining read...");

		//sets grid size to the input values
		grid = new Site[counter];		

		br = new BufferedReader(new FileReader(landFile));
		readMapData(grid, br);
		br.close();

		System.out.println(" Loaded");
		return grid;				
	}

	/**
	 * 
	 * @param grid The Site array to change
	 * @param br The Buffered reader associated with a ".pop" or ".land" File
	 * @throws NumberFormatException Dealt with by user in higher level classes
	 * @throws IOException Dealt with by user in higher level classes
	 */
	private static void readMapData(Site[] grid, BufferedReader br) throws NumberFormatException, IOException{
		int onePercent = grid.length/100;

		String line;
		int i = 0; 
		while((line = br.readLine()) != null){

			//reads the current line and splits the values by white space,
			//storing them in a string array
			String[] currentline = line.split(" ");				

			if(grid[i] == null){
				grid[i] = new Site(Integer.parseInt(currentline[0]), Integer.parseInt(currentline[1]), defaultK);
			}
			
			//Read in line (X, Y, CIV, dn, CIV, dn, ...)
			if(currentline.length > 2){
				grid[i].setK(Double.parseDouble(currentline[2]));
				grid[i].setK0(Double.parseDouble(currentline[2]));

				for (int j = 3; j < currentline.length-1; j+=2) {
					grid[i].addPopulation(Integer.parseInt(currentline[j]), Double.parseDouble(currentline[j+1]));
				}
			}
			++i;
			if(i%onePercent ==0) System.out.print(">");
		}
		System.out.println();
	}

	/**
	 * Sets the parameters from an input file. Where String-Double pairs are on each line. "#" and "//" are comments
	 * @throws IOException
	 */
	public static Parameters readParameters(String paramFile) throws IOException{
		Parameters p = new Parameters();
		BufferedReader br = new BufferedReader(new FileReader(new File(paramFile)));
		String line;
		while((line = br.readLine()) != null){
			if(line.startsWith("#") || line.startsWith("//"))
				continue;
			p.addPairs(line);
		}
		br.close();
		return p;
	}

	/**
	 * Reads the strategies of the civilisations from a file where there is one line per civilistion<br>
	 * The line should read "Name(String) Birth-rate(double) Intellect(double) Aggression(double)". Space separated
	 * @param civilisationFile Location of the file
	 * @return An arraylist of civilisation to be used by a Model
	 */
	public static ArrayList<Civilisation> readCivilisations(String civilisationFile) throws IOException {
		ArrayList<Civilisation> civs = new ArrayList<Civilisation>();
		BufferedReader br = new BufferedReader(new FileReader(new File(civilisationFile)));
		String line;
		String[] currentline;
		int counter = 0;
		while((line = br.readLine()) != null){
			currentline = line.split(" ");
			civs.add(new Civilisation(currentline[0], counter, 
					Double.parseDouble(currentline[1]), 
					Double.parseDouble(currentline[2]), 
					Double.parseDouble(currentline[3])));
			counter++;
		}
		br.close();
		return civs;
	}
	
}


/**
 * 
 */
package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import main.data.Civilisation;
import main.data.InteractionMatrix;
import main.data.Parameters;
import main.data.Site;
import main.runnables.SectionCopier;
import main.runnables.SectionIterator;

/**
 * @author s0941897
 * 
 */
public abstract class Model {

	//Static 
	public static Model model;

	/**
	 * The DEFAULT number of threads for the pool 
	 */
	protected static final int defaultNoThreads = 100;

	/**
	 * The default rates to be used G (natural environmental recovery), D_k, D_people, DT, 
	 *  alpha (point where overpop has an effect) (Note: theta = 0.5;
	 *  {0    1    2    3     4        5
	 *  {Beta Dk   D    Dt    alpha   mutate}   
	 */
	protected static final String defaultParameters = "beta 0.5 Dk 0.5 D 7.0 dt 0.01 alpha 1.1 mutate 1000 d0 0.02 b0 0.1 lambda 0.01 sm 50";
	
	// ********* Variables *********************

	/**
	 * The Map
	 */
	protected Site[] map;

	/**
	 * The intermediate temporary map to save the previous step in
	 */
	protected Site[] workingMap;

	/**
	 * List of civilisations with names, indices and growth rates. Also stores
	 * the average density
	 */
	protected ArrayList<Civilisation> civilisations;

	/**
	 * A wrapped up double[][] storing the interaction coefficients
	 */
	protected InteractionMatrix matrix;

	/**
	 * Stores the other rates such as diffusion rates, etc. S, D0, D, DT
	 */
	protected Parameters parameters;

	/**
	 * The pool of processors to step the simulation with
	 */
	private ThreadPoolExecutor executor;

	protected ExecutorCompletionService<?> ecs;

	/**
	 * The number of threads in the pool
	 */
	private int numberOfThreads;
	
	private int sitesPerThread;
	
	protected int steps = 0;

	// ******* Constructors ********************
	/**
	 * Create an empty model with the ExecutorService, Thread pool and global pointer set. 
	 */
	protected Model(int numberOfThreads){
		//Create Exectors
		if(executor!=null)
			executor.shutdownNow();
		
		executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		ecs = new ExecutorCompletionService<Object>(executor);
		
		//Set the number of threads to be used
		this.numberOfThreads = numberOfThreads;
		
		//Init the arraylists
		civilisations = new ArrayList<Civilisation>();
		parameters = new Parameters();
		
		//Make the static reference of the model equal to this instance of the model
		Model.model = this;
	}
	
	/**
	 * Creates a model with the map created by the given cartographer	
	 * @param mapMaker The Cartographer
	 */
	public Model(Cartographer mapMaker){
		this(defaultNoThreads, mapMaker);
	}
	
	public Model(int numberOfThreads, Cartographer mapMaker){
		this(numberOfThreads);
		// Remove pointers to old data and garbage collect
		clean();
		
		parameters = new Parameters(defaultParameters);
		
		setMap(mapMaker.getMap());

		// Initialise the civilisations
		this.civilisations = mapMaker.getCivilisations();
		Civilisation.setNextIndex(this.civilisations.size());

		mapMaker = null;

		// Use a default matrix and rates
		this.matrix = new InteractionMatrix(this.civilisations.size());
		this.matrix.create(civilisations);
	}
	
	/**
	 * Load a model from four human readable files. The latter two are not required. <br>
	 * See the documentation for use of the files
	 * @param landFileLocation The file containing land sites. (Required) 
	 * @param parameterFile The line separated file of parameters. (Default parameters will try to be used if null)
	 * @param civilisationFile The line separated file of civilisation strategies. (Random civs created if null)
	 */
	public Model(String landFileLocation, String parameterFile, String civilisationFile){
		this(defaultNoThreads);
		this.matrix = new InteractionMatrix(0);
		//Load Parameters
		if(parameterFile == null){
			parameters = new Parameters(defaultParameters);
		}else{
			loadParameters(parameterFile);
		}
		//Load the Map
		if(landFileLocation == null){
			throw new NullPointerException("A map file is required");
		}else{
			loadMap(landFileLocation);
		}
		//Add the civilisations
		if(civilisationFile == null){
			int numberOfCivs = 0;
			for (int i = 0; i < map.length; i++) 
				if(map[i].size()>numberOfCivs)
					numberOfCivs=map[i].size();
			System.out.println("No Civilisation file provided. "+numberOfCivs+" civilisations will have random strategies");
			for (int i = 0; i < numberOfCivs; i++) 
				civilisations.add(new Civilisation());
		}else{
			loadCivilisations(civilisationFile);
		}
	}
	
	@SuppressWarnings("unchecked")
	public Model(String modelFile){
		this(defaultNoThreads);
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(new File(modelFile)));
			setMap((Site[]) in.readObject());
			Cartographer.findNeighbours(map);
			civilisations = (ArrayList<Civilisation>) in.readObject();
			parameters = (Parameters) in.readObject();
			steps = in.readInt();
			this.matrix = new InteractionMatrix(civilisations.size());
			this.matrix.create(civilisations);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * Load the map from a text files. 
	 * @param landFileNoExtention The location of the file
	 * @see Input.loadMapData()
	 */
	protected void loadMap(String landFileNoExtention) {
		try {
			setMap(Input.loadMapData(landFileNoExtention));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Cartographer.findNeighbours(map);
	}

	/**
	 * Load the rates and other data (not map) from file.
	 * @param paramFile
	 */
	protected void loadParameters(String paramFileLocation){
		try {
			parameters = Input.readParameters(paramFileLocation);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load the civilisations where each is on a separate line in the given file
	 * @param civilisationFile The file where each line has "Name B I A"
	 */
	protected void  loadCivilisations(String civilisationFile) {
		try {
			civilisations = Input.readCivilisations(civilisationFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// ******* Getters ********************
	/**
	 * @return the map
	 */
	public Site[] getMap() {
		return map;
	}

	/**
	 * @return the civilisations
	 */
	public ArrayList<Civilisation> getCivilisations() {
		return civilisations;
	}

	/**
	 * @return the matrix
	 */
	public InteractionMatrix getMatrix() {
		return matrix;
	}

	/**
	 * @return the rates
	 */
	public Parameters getParameters() {
		return parameters;
	}

	// ******* Setters ********************

	/**
	 * @param map
	 *            the map to set
	 */
	public void setMap(Site[] map) {
		this.map = map;
		initWorkingMap();
		updateSitesPerThread();
	}
	
	/**
	 * Should be called when there is a map set and after the number of threads has been changed
	 */
	public void updateSitesPerThread(){
		if(numberOfThreads>map.length)
			sitesPerThread = map.length;
		else
			this.sitesPerThread = map.length/numberOfThreads;
		
		System.out.println("Sites: "+map.length+" NumberOfThreads: "+ numberOfThreads + " Sites per Thread: "+sitesPerThread);
	}

	/**
	 * @param civilisations
	 *            the civilisations to set
	 */
	public void setCivilisations(ArrayList<Civilisation> civilisations) {
		this.civilisations.clear();
		this.civilisations.addAll(civilisations);
	}

	/**
	 * @param matrix
	 *            the matrix to set
	 */
	public void setMatrix(InteractionMatrix matrix) {
		this.matrix = matrix;
	}

	/**
	 * Sets all of the rates
	 * @param rates
	 *            rates to set
	 */
	public void setParameters(Parameters rates) {
		this.parameters = rates;
	}
	
	
	/**
	 * Set the number of threads. This kills the current execution. Best done before calling stepSimulation
	 * @param n the number of threads
	 */
	public void setNumberOfThreads(int n){
		this.numberOfThreads = n;
		
		try {
			updateSitesPerThread();
		}catch (NullPointerException e) {
			System.err.println("Number of threads changed. Not able to calculate threads per site. Model.map == null");
			System.exit(2);
		}
	}

	// ******* Methods ********************

	/**
	 * Returns a copy of the map
	 */
	public Site[] copyMap() {
		Site[] cp = new Site[map.length];
		for (int i = 0; i < map.length; i++) {
			cp[i] = new Site(map[i]);
		}
		return cp;
	}

	/**
	 * Set all fields to null and garbage collect
	 */
	public void clean() {
		map = null;
		civilisations = new ArrayList<Civilisation>();
		matrix = null;
		parameters = null;
		System.gc();
	}
	
	public void dumpMap(){
		for (int i = 0; i < map.length; i++) {
			System.out.println(map[i].getX()+" "+map[i].getY()+" "+map[i].getTotal());
		}
	}
	
	/**
	 * Returns an array of the civilisations totals
	 * @return civilisations totals
	 */
	public double[] subTotalPopulation() {
		double[] output = new double[civilisations.size()];

		for (int i = 0; i < output.length; i++) {
			output[i] = civilisations.get(i).getTotalPopulation();
		}
		return output;
	}
	
	/**
	 * Returns the grand total of all civilisations
	 * @return the grand total population
	 */
	public double totalPopulation(){
		double[] tots = subTotalPopulation();
		double out = 0.0;
		for (int i = 0; i < tots.length; i++) {
			out += tots[i];
		}
		return out;
	}

	public double totalK() {
		double output = 0.0;
		for (int j = 0; j < map.length; j++) {
			output += map[j].getK();
		}

		return output;
	}

	/**
	 * Iterate for every site in the map. <br>
	 * This is the same as simulationStep but not multithreaded. 
	 * ONLY use this method unless there is ONLY one core available
	 */
	//@Deprecated
	public void iterateAll(){

		for (int i = 0; i < map.length; i++) {
			workingMap[i].update(map[i]);
		}
		for (int i = 0; i < map.length; i++) {
			iterate(i);
		}
	}

	/**
	 * ****************************************************************************************************<br>
	 * Arguably the most important method. Steps the simulation forwards one step in a multi-threaded manner. <br>
	 * ****************************************************************************************************<br>
	 */
	public void simulationStep(){
		//Optional calculation before the iterate method
		preIterate();
		//Copy to current timestep to the working map in parallel
		startCopiers();
		//Set up the jobs on the thread pool
		startIterators();
		//Optional calculations after the interate method
		postIterate();
				
		steps++;
		
		executor.purge();
	}
	
	/**
	 * Starts the SectionCopiers and waits for them to complete. This copies the current map to the workingMap
	 */
	private void startCopiers(){
		int startNo = 0;
		int endNo = sitesPerThread;
		for(int i = 0; i < numberOfThreads; i++){
			ecs.submit(new SectionCopier(startNo, endNo),null);
			startNo += sitesPerThread;
			endNo += sitesPerThread;
			if(i== (numberOfThreads-2)) endNo = map.length;
		}
		for (int i = 0; i < numberOfThreads; i++) {
			try {
				ecs.take().get();
			} catch (InterruptedException e) {
			} catch (ExecutionException e) {
			}
		}
		
	}
	
	/**
	 * Starts the SectionIterators and waits for them to complete. This steps the simulation forward in time
	 */
	private void startIterators(){
		int startNo = 0;
		int endNo = sitesPerThread;
		for(int i = 0; i < numberOfThreads; i++){
			ecs.submit(new SectionIterator(startNo, endNo),null);
			startNo += sitesPerThread;
			endNo += sitesPerThread;
			if(i== (numberOfThreads-2)) endNo = map.length;
		}
		for (int i = 0; i < numberOfThreads; i++) {
			try {
				ecs.take().get();
			} catch (InterruptedException e) {
			} catch (ExecutionException e) {
			}
		}
	}
	
	private void initWorkingMap(){
		workingMap = new Site[map.length];
		for (int i = 0; i < workingMap.length; i++) {
			workingMap[i] = new Site(map[i].getX(),map[i].getY(), map[i].getK());
		}
		
		Cartographer.findNeighbours(workingMap);
	}
	
	/**
	 * Update the range of the working map to the values in the current time step.
	 * @see SectionCopier
	 * @param start The start index of the map section
	 * @param end The end index of the map section
	 */
	public void updateWorkingMap(int start, int end){
		for (int i = start; i < end; i++) {
			workingMap[i].update(map[i]);
		}
	}
	
	// ****** Abstract Methods **************
	/**
	 * Optional method which IS called before the iterate() method. <br>
	 * super.preIterate() does nothing.
	 */
	protected void preIterate(){};
	/**
	 * Optional method which IS called after the iterate() method. <br>
	 * super.postIterate() does nothing.
	 */
	protected void postIterate(){};

	/**
	 * Must be implemented for the model to do anything.<br>
	 * @param siteIndex The site to update
	 */
	public abstract void iterate(int siteIndex);
	
;

}

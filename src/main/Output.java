package main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class Output {

	/**
	 * Write the Model to a binary file. Used for quick loading of a model. 
	 * @param m The model to output
	 * @param outputLocation The location of the file. 
	 */
	public static void modelToFile(Model m, String outputLocation){
		
		File output = new File(outputLocation);
		
		//Clear the pointers to avoid stack overflow
		for (int i = 0; i < m.getMap().length; i++) {
			m.getMap()[i].setNeighbours(null);
		}
		
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(output));
			out.writeObject(m.getMap());
			out.writeObject(m.getCivilisations());
			out.writeObject(m.getParameters());
			out.writeInt(m.steps);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		Cartographer.findNeighbours(m.getMap());
		
	}
	
}

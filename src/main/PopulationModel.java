package main;

import java.util.concurrent.ExecutionException;

import main.data.Civilisation;
import main.data.Site;
import main.runnables.CivilisationUpdater;

/**
 * @author s0941897
 * 
 */
public class PopulationModel extends Model {


	/**
	 * Load a model from four human readable files. The latter two are not required. <br>
	 * See the documentation for use of the files
	 * @param landFileLocation The file containing land sites. (Required) 
	 * @param parameterFile The line separated file of parameters. (Default parameters will try to be used if null)
	 * @param civilisationFile The line separated file of civilisation strategies. (Random civs created if null)
	 */
	public PopulationModel(String landFileNoExtention, String parameterFile, String civilisationFile) {
		super(landFileNoExtention, parameterFile, civilisationFile);
	}

	/**
	 * Creats a square map PopulationModel	
	 * @param seed Random number generator seed
	 * @param longestSide The length of the square
	 * @param civs The number of civilisation to put on the map. 
	 */
	public PopulationModel(Cartographer mapMaker){
		super(mapMaker);
	}

	public PopulationModel(int numberOfThreads, Cartographer mapMaker){
		super(numberOfThreads, mapMaker);
	}

	public PopulationModel(String modelFile){
		super(modelFile);
	}

	@Override
	/**
	 * The calculation to perform before the iterate() method.<br>
	 * In the case of PopulationModel, the civilisations are updated.
	 */
	protected void preIterate() {
		for (int i = 0; i < civilisations.size(); i++) {
			ecs.submit(new CivilisationUpdater(civilisations.get(i)), null);
		}
		for (int i = 0; i < civilisations.size(); i++) {
			try {
				ecs.take().get();
			} catch (InterruptedException e) {
			} catch (ExecutionException e) {
			}
		}

	}

	public void iterate(int i) {
		birthdeathcompetition(i);
		diffuse(i);
		updateK(i);
		diffuseK(i);
	}

	@Override
	protected void postIterate() {
		if(Cartographer.r.nextInt((int)parameters.get("mutate")) == 1)
			mutate();
	}

	/**
	 * Births, Deaths and Competitions for site i<br>
	 * "dt" is a required parameter
	 * @param i The site to update
	 */
	protected void competitiveLV(int i) {

		// Copy site so that all Nk are from the previous timestep (i.e. not
		// favouring low k civilisations

		for (int k = 0; k < civilisations.size(); k++) {
			//If no one here then move on
			if(civilisations.get(k).isDead())
				continue;

			double sum = 0.0;
			for (int others = 0; others < civilisations.size(); others++) {
				//If the other civ is dead then skip them.
				if(civilisations.get(others).isDead())
					continue;
				sum += (matrix.getInteraction(k, others) * workingMap[i].getPopulation(others));
			}


			double dn = civilisations.get(k).getBirthrate() * workingMap[i].getPopulation(k) * (1 - ((sum / workingMap[i].getK())));
			dn -= parameters.get("d0") * workingMap[i].getPopulation(k);

			map[i].addPopulation(k, parameters.get("dt") * dn);
		}
	}

	/**
	 * Based on the Cultral Hitchiking paper<br>
	 * "sm" is a required parameter
	 * @param i The site to update
	 */
	protected void nonLogistic(int i){
		double s = (parameters.get("sm") * workingMap[i].getK())/(parameters.get("sm") + workingMap[i].getK());
		double n0 = 1.0 + workingMap[i].getK() / parameters.get("sm");//Am = 1.0
		double n0s = n0 * (1+s);

		for (int k = 0; k < civilisations.size(); k++) {
			double r = Math.sqrt(civilisations.get(k).getBirthrate()*parameters.get("b0")/parameters.get("d0")) * s / (s+1.0);
			if(civilisations.get(k).isDead())
				continue;

			double arctau = parameters.get("d0")*(1.0 + s)*(r-1.0)*(n0s*(r + 1.0) + workingMap[i].getPopulation(k));
			arctau /= s*(n0s + workingMap[i].getPopulation(k));

			double dn = arctau*workingMap[i].getPopulation(k)*(1.0 - workingMap[i].getPopulation(k)/(n0s + workingMap[i].getPopulation(k)));

			map[i].addPopulation(k, dn);
		}
	}

	/**
	 * Update the population using the following equation: <br>
	 * rx(1-N/K) + sum(xy(Ax*x - Ay*y) where N is total, K is carrying capacity, Ai is aggression of i and the sum is over all civilisations.<br>
	 * "lambda" is a required parameter
	 * @param i The index of the site to update
	 */
	protected void birthdeathcompetition(int i) {
		for (int k = 0; k < civilisations.size(); k++) {
			//If no one here then move on
			if(civilisations.get(k).isDead())
				continue;

			double sum = 0.0;
			for (int others = 0; others < civilisations.size(); others++) {
				//If the other civ is dead then skip them.
				if(civilisations.get(others).isDead())
					continue;

				double diff = workingMap[i].getPopulation(k)*civilisations.get(k).getAggression();
				diff -= workingMap[i].getPopulation(others)* civilisations.get(others).getAggression();

				sum += workingMap[i].getPopulation(others)*workingMap[i].getPopulation(k)*diff;
			}

			double sitetotal = workingMap[i].getTotal();
			double dn;

			//b0 * bi * xi
			dn = parameters.get("b0")*civilisations.get(k).getBirthrate()*workingMap[i].getPopulation(k);
			//* (1 - N/k)
			dn *= (1.0-(sitetotal/workingMap[i].getK()));
			// - d0 * xi
			dn -= parameters.get("d0")*workingMap[i].getPopulation(k);
			//Remove the cometition effects
			dn += parameters.get("lambda")*sum/(sitetotal*sitetotal);

			//Change the population on the actual copy of the map. 
			map[i].addPopulation(k, parameters.get("dt") * dn);
		}
	}

	/**
	 * Move people around the grid based on square neighbour interactions<br>
	 * "Dp" and "dt" are required parameters.
	 * @param i The site to update
	 */
	protected void diffuse(int i) {

		for (int k = 0; k < civilisations.size(); k++) {

			double nn = 0.0;
			for(Site neighbour : workingMap[i].getNeighbours()){
				nn += neighbour.getPopulation(k);
			}

			double dn = parameters.get("D") * (nn - workingMap[i].getNeighbours().length * workingMap[i].getPopulation(k));

			map[i].addPopulation(k, parameters.get("dt") * dn);

		}
	}

	/**
	 * Update the carrying capacity of site i <br>
	 * "alpha", "beta" and "dt" are needed in parameters
	 * @param i The site to update
	 */
	protected void updateK(int i) {

		//BETA = rates[0] = 1.25 default
		//alpha = rates[4] = 1.25 default
		// beta = 1.25 default

		double k = workingMap[i].getK();
		double nt = workingMap[i].getTotal();
		double dk = 0.0;

		//No people update if no people
		if(nt!=0.0){
			//Calculated weighted average of intelligence
			double g = 0.0;
			for (int j = 0; j < civilisations.size(); j++) {
				g += (workingMap[i].getPopulation(j)/nt)*civilisations.get(j).getIntellect();
			}
			dk += g*(parameters.get("alpha")*k - nt)*(nt / k);
		}

		dk -= parameters.get("beta")* (k - workingMap[i].getK0());
		dk *= parameters.get("dt");
		map[i].addK(dk);

	}

	/**
	 * Diffuse the carrying capacity. The parameter "dt" and "Dk" are needed.
	 * @param i
	 */
	protected void diffuseK(int i){
		double nn = 0.0;
		for(Site neighbour : workingMap[i].getNeighbours()){
			nn += neighbour.getK();
		}

		double dk = parameters.get("Dk") * (nn - workingMap[i].getNeighbours().length * workingMap[i].getK());

		dk *= parameters.get("dt");

		map[i].addK(dk);
	}

	/**
	 * Mutate the site and create a random civilisation
	 * @param i The site to mutate
	 */
	protected void mutate() {

		Civilisation newciv = new Civilisation();
		System.out.println("+ "+newciv);

		civilisations.add(newciv);
		double totalConverts = 0.0;
		double converts = 0.0;

		//Get a site with people on it
		int siteIndex = Cartographer.r.nextInt(map.length);
		while(workingMap[siteIndex].getTotal()<=0.0)
			siteIndex = Cartographer.r.nextInt(map.length);

		//Convert people on the site
		for (int j = 0; j < workingMap[siteIndex].size(); j++) {
			converts = workingMap[siteIndex].getPopulation(j); // Cartographer.r.nextDouble()
			map[siteIndex].addPopulation(j, -converts);
			totalConverts += converts;
		}
		map[siteIndex].addPopulation(newciv.getIndex(), totalConverts);

		//Convert people on the site's neighbours
		for (int i = 0; i < map[siteIndex].getNeighbours().length; i++) {
			totalConverts = 0.0;
			Site site = map[siteIndex].getNeighbours()[i];
			for (int j = 0; j < site.size(); j++) {
				converts = site.getPopulation(j); // Cartographer.r.nextDouble()
				site.addPopulation(j, -converts);
				totalConverts += converts;
			}
			site.addPopulation(newciv.getIndex(), totalConverts);
		}
	}


	/**
	 * Sets all K to zero
	 * 
	 */
	public void clearK() {
		for (int i = 0; i < map.length; i++) {
			map[i].setK(0.0);

		}
	}

}

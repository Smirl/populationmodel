/**
 * 
 */
package main.data;

import java.awt.Color;
import java.io.Serializable;

import main.Cartographer;
import main.Model;

/**
 * An object to represent a civilisation. Stores growth rates, name and average population density
 * 
 * @author s0941897
 *
 */
public class Civilisation implements Serializable {

	private static final long serialVersionUID = -4592358689143689265L;

	public static final double MIN_POPULATION = 1.0;
	
	//Static variables
	private static int nextIndex = 0;

	private static String[] names = {"Barbarians","Mayans","Romans","Britons","Greeks", "Trojans", "Persians", "Chinese", "Vikings"};

	//Instance Variables
	private String name;
	private int index;
	private boolean isDead = false;
	private Color color;

	//The EGT relevant variables
	private double birthrate;
	private double intellect;
	private double aggression;

	//Data concerning the civilisation
	private double averageDensity;
	private double totalPop;

	/**
	 * Create a civilisation when the model is known
	 */
	private void initialise(String name, int index, double birth, double intellect, double aggression) {
		this.name = name;
		this.index = index;

		this.birthrate = birth;
		this.intellect = intellect;
		this.aggression = aggression;
		
		setColor();
		this.totalPop = 0.0;

		nextIndex++;

		if(Model.model != null){
			addToSites();
			Model.model.getMatrix().addCivilisation(this);
		}
	}

	/**
	 * Create a civilisation when the model is known. 
	 * @param name
	 * @param index
	 * @param birth
	 * @param intellect
	 * @param aggression
	 */
	public Civilisation(String name, int index, double birth, double intellect, double aggression){
		initialise(name, index, birth, intellect, aggression);
	}
	
	/**
	 * Create a civilisation when the model is not known or unchanged and a name is not important.
	 */
	public Civilisation(int index, double birth, double intellect, double aggression){
		initialise(randomName(index),index, birth, intellect, aggression);
	}

	/**
	 * Create a new Civilisation with the set strategy
	 * @param growth
	 * @param intellect
	 * @param aggression
	 */
	public Civilisation(double birth,double intellect, double aggression){
		initialise(randomName(nextIndex), nextIndex, birth, intellect, aggression);
	}

	/**
	 * Create a random Civilisation with
	 */
	public Civilisation(){
		birthrate = Cartographer.r.nextDouble();
		intellect  = Cartographer.r.nextDouble();
		aggression = Cartographer.r.nextDouble();
		
		double scale = 1.0 / (birthrate+intellect+aggression);
		
		birthrate *= scale;
		intellect *= scale;
		aggression *=scale;
		
		initialise(randomName(nextIndex),nextIndex,birthrate,intellect,aggression);
	}

	//********** Getters and Setters **********************
	//Getters
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public int getIndex(){
		return index;
	}

	/**
	 * @return the growthRate
	 */
	public double getBirthrate() {
		return birthrate;
	}
	/**
	 * @return the intellect
	 */
	public double getIntellect() {
		return intellect;
	}
	/**
	 * @return the aggression
	 */
	public double getAggression() {
		return aggression;
	}
	
	/**
	 * Return the color of the object. where RGB refers to Aggression,Growth,Intellect
	 * @return The Color
	 */
	public Color getColor(){
		return color;
	}

	/**
	 * @return the averageDensity
	 */
	public double getAverageDensity() {
		return averageDensity;
	}

	/**
	 * @return the total population
	 */
	public double getTotalPopulation(){
		return totalPop;
	}

	//Setters
	
	/**
	 * Can be used to set the strategy of the civilisation after it has been created.
	 * The contraint that the three numbers must add to 1.0 is not enforced but should be obeyed unless deliberate. 
	 * Note: if sum of numbers is greater than 1. the colour of the civilisation is black;
	 * @param birthrate 
	 * @param intellect
	 * @param aggression
	 */
	public void setStrategy(double birthrate, double intellect, double aggression){
		this.birthrate = birthrate;
		this.intellect = intellect;
		this.aggression = aggression;
		
		if(birthrate+intellect+aggression > 1.0){
			color = Color.BLACK;
		}else{
			setColor();
		}
	}
	
	/**
	 * Set the color of the civilisation
	 */
	private void setColor(){
		this.color = new Color((float)this.aggression, (float)birthrate, (float)this.intellect);
	}


	//************ Methods ********************************

	/**
	 * Add 0.0 people on every site for this civilisation
	 */
	private void addToSites(){
		for (int i = 0; i < Model.model.getMap().length; i++) {
			Model.model.getMap()[i].addPopulation(index, 0.0);
		}
	}

	/**
	 * Set the value of the density of this civilisation to zero on every site.
	 */
	private void removeFromSites(){
		for (int i = 0; i < Model.model.getMap().length; i++) {
			Model.model.getMap()[i].setPopulation(index, 0.0);
		}
	}

	public void update(){
		calculateTotalPopulation();
		
		//If the Civilisation's total is less than 1.0. Then kill the civilisation
		if(totalPop < MIN_POPULATION){
			killCivilisation();
		}
	}
	
	/**
	 * Remove the civilisation from existence. No people anywhere and is dead = true.
	 */
	private void killCivilisation(){
		isDead = true;
		removeFromSites();
		totalPop = 0.0;
		System.out.println("- "+name);
	}

	/**
	 * Is the civilization dead.
	 * @return The status of the civilization
	 */
	public boolean isDead(){
		return isDead;
	}
	
	public void calculateAverageDensity(Site[] map){
		averageDensity = 0.0;
		double numLandAreas = 0;

		for(int i=0; i<map.length; i++){
			averageDensity += map[i].getPopulation(index);
			numLandAreas += 1.0;
		}
		averageDensity = averageDensity/numLandAreas;
	}

	public double calculateTotalPopulation(){
		if(Model.model==null || isDead)
			return 0.0;
		totalPop = 0.0;
		for (int i = 0; i < Model.model.getMap().length; i++) {
			totalPop += Model.model.getMap()[i].getPopulation(index);
		}

		return totalPop;
	}

	//************ Static Methods ********************************
	/**
	 * Return the next available index based on the current number of civilisation in the model
	 */
	public static int nextAvailableIndex(){
		return nextIndex;
	}

	/**
	 * Calculate a random name of a civilisation with the index included.
	 * @param index The index of the civilisation
	 * @return A random String appended with the index given. 
	 */
	public static String randomName(int index){
		return names[Cartographer.r.nextInt(names.length)] + index;
	}

	/**
	 * Set the model that all civilisations live in.
	 */
	public static void setModel(Model m){
		Model.model = m;
	}

	/**
	 * Set the next available index to a new value. Call reset to return it to zero
	 * @param i the next unique index
	 */
	public static void setNextIndex(int i){
		nextIndex = i;
	}

	/**
	 * Reset the starting index if a new model is created
	 */
	public static void reset(){
		nextIndex = 0;
	}

	@Override
	public String toString() {
		return String.format("%s\t\tR:%.4f I:%.4f A:%.4f %s", name,birthrate,intellect,aggression,(isDead ? "DEAD" : "NOT DEAD"));
	}

}

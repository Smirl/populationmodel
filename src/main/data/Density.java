package main.data;

import java.io.Serializable;

/**
 * Used in the Site density ArrayList. This is in place of using the Double object which is not mutable and 
 * could lead to {@link OutOfMemoryError} if the garbage collection is too slow.  
 */
public class Density implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private double value;
	
	/**
	 * Initialize the value to 0.0
	 */
	public Density(){
		value = 0.0;
	}
	
	/**
	 * Initialize the value to the given double
	 * @param initialValue The initial value of the Density
	 */
	public Density(double initialValue){
		value = initialValue;
	}
	
	/**
	 * Get the value of the density
	 * @return The value
	 */
	public double get(){
		return value;
	}
	
	/**
	 * Set the value of the density
	 * @param newValue 
	 */
	public void set(double newValue){
		value = newValue;
	}
	
	@Override
	public boolean equals(Object obj) {
		return ( super.equals(obj) || value==((Density) obj).get() );
	}
	
}

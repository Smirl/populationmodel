/**
 * 
 */
package main.data;

import java.util.ArrayList;
import java.util.Random;

import main.Model;

/**
 * An object to represent the interaction of two civilisations on each other. 
 * 
 * @author Alex Williams
 */
public class InteractionMatrix {
	
	/**
	 * The offset used to calculate the interaction between two Civilisations. <br>
	 * If OFFSET>0 then negative effects occur
	 */
	//private static double OFFSET = 1.0;

	/**
	 * The Matrix of interations
	 */
	double[][] matrix;

	/**
	 * Creates a new square matrix which is set all elements to 1.0; 
	 * @see InteractionMatrix.setInterations(String val)
	 * @category Constructor
	 */
	public InteractionMatrix(int NoOfPopulations) {
		matrix = new double[NoOfPopulations][NoOfPopulations];
		createIdentity();
	}

	/**
	 * Get an individual interaction. This is how <code>toBeAffected</code> is affected by
	 * <code>affectedBy</code>.
	 * 
	 * @param toBeAffected index of civ to be affected
	 * @param affectedBy index of civ for above to be affected by
	 * @category Getter
	 */
	public double getInteraction(int toBeAffected, int affectedBy){
		return matrix[toBeAffected][affectedBy];
	}

	/**
	 * Set all of the interations at once with a string in the form of:
	 * "1.0,2.0,3.0 1.0,5.0,3.0 8.0,2.0,3.0". Please note the commas (no space)
	 * between columns and the space (no comma) between rows.
	 * 
	 * @param val The string to be parsed into values
	 * @category Setter
	 */
	public void setInteractions(String val) {
		String[] rows = val.split(" ");
		for (int i = 0; i < rows.length; i++) {
			if (rows.length != matrix.length)
				throw new IllegalArgumentException(
				"Incorrent number of arguments for this column");
			String[] args = rows[i].split(",");
			for (int j = 0; j < args.length; j++) {
				if (args.length != matrix[0].length)
					throw new IllegalArgumentException(
					"Incorrect number of arguments in this row");
				matrix[i][j] = Double.parseDouble(args[j]);
			}
		}
	}

	/**
	 * Set an individual interaction. This is how <code>toBeAffected</code> is affected by
	 * <code>affectedBy</code> by ammout <code>val</code>
	 * 
	 * @param toBeAffected index of civ to be affected
	 * @param affectedBy index of civ for above to be affected by
	 * @param val the ammount (multiplier)
	 * @category Setter
	 */
	public void setInteraction(int toBeAffected, int affectedBy, double val) {
		matrix[toBeAffected][affectedBy] = val;
	}
	
	//***********************************************************************************
	// Helper Methods
	//***********************************************************************************
	
	/**
	 * Called by the Civilisation constructor to add the new lines to the matrix. 
	 * @param the newest civilisation
	 */
	public void addCivilisation(Civilisation newCiv){
		//make the matrix larger
		double[][] temp =  new double[matrix.length + 1][matrix.length + 1];
		
		Civilisation a ,b;
		
		for (int i = 0; i < temp.length; i++) {
			for (int j = 0; j < temp.length; j++) {
				//Fill in the already calculated lines
				if(i<matrix.length && j<matrix.length){
					temp[i][j] =  matrix[i][j];
				}
				//Make sure to get the correct civilisations then add the new lines
				else{

					if(i!=newCiv.getIndex())
						a = Model.model.getCivilisations().get(i);
					else
						a = newCiv;
					
					if(j!=newCiv.getIndex())
						b = Model.model.getCivilisations().get(j);
					else
						b = newCiv;
					
					temp[i][j] = calculateInteraction(a,b);
				}
			}
		}
		//The temp matrix can now become the instance's
		matrix = temp.clone();
	}
	
	protected double calculateInteraction(Civilisation a, Civilisation b){
		//return Math.exp(-1.0 * (a.getAggression() - b.getAggression() + OFFSET) / 2.0) - Math.exp(OFFSET/2.0);
		return 1.0 + (b.getAggression() - a.getAggression());
		//return Math.max(1.0, 1.0 + (b.getAggression() - a.getAggression()));
	}

	//***********************************************************************************
	// Matrix creators
	//***********************************************************************************

	/**
	 * The most useful. Creates a matrix where the civilisation's strategies are used to compute the 
	 * matrix elements. Exact rules for this are in calculateInteraction(Civilisation, Civilisation)
	 * @see calculateInteraction(Civilisation, Civilisation)
	 */
	public void create(ArrayList<Civilisation> civs){
		matrix = new double[civs.size()][civs.size()];
		for (int i = 0; i < civs.size(); i++) {
			for (int j = 0; j < civs.size(); j++) {
				matrix[i][j] = calculateInteraction(civs.get(i), civs.get(j));
			}
		}
		System.out.println("Interaction Matrix\n"+this);
	}

	/**
	 * Create a random IM with on diagonals = 1.0 and off diagonals greater than 1.0 (aggressive)
	 * @param seed for the random number generator
	 * @param maximum the maximum interaction. All others will be scaled between this and 0,0
	 */
	public void createNonSelfAggressive(int seed, double maximum){
		Random random = new Random(seed);
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				matrix[i][j] = random.nextDouble()*maximum + 1.0;
				if(i==j) matrix[i][j] = 1.0;
			}
		}
		random = null;
	}

	/**
	 * Creates a matrix where on diagonal terms are 1.0. All others 0.0
	 */
	public void createIdentity(){
		for (int i = 0; i < matrix.length; i++) 
			for (int j = 0; j < matrix[0].length; j++) 
				if(i==j)
					matrix[i][j] = 1.0;
				else
					matrix[i][j] = 0.0;
	}

	public void createAggressive(int seed, double maximum){
		Random random = new Random(seed);
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				matrix[i][j] = random.nextDouble()*maximum + 1.0;
			}
		}
		random = null;
	}

	/**
	 * Creates a matrix where all elements are equal to the value given
	 * @param val The value that all elements are equal to
	 */
	public void createEquality(double val){
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				matrix[i][j] = val;
			}
		}
	}

	//***********************************************************************************
	// Overrides
	//***********************************************************************************

	@Override
	public String toString() {
		String out = "";

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				out = out + " " + String.format("% 5.2f", matrix[i][j]);
			}
			out = out + "\n";
		}

		return out;
	}
	
}

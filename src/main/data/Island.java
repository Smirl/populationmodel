package main.data;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import main.Cartographer;

/**
 * A collection of sites which touch one another
 * @author s0941897
 *
 */
public class Island {

	public static Color[] colours = new Color[]{Color.RED,Color.ORANGE,Color.YELLOW,Color.CYAN,Color.MAGENTA,Color.BLACK};

	public final int ID;
	public Site startSite;
	public HashMap<String,Site> landMass;
	public ArrayList<Site> toExplore;

	/**
	 * Make an island and add the startsite to both the <code>landMass</code> (island land sites) and the <code>toExplore</code>
	 * @param startSite the site which to start this island from
	 * @param id A unique id of the island (array index for example). Used to Color this island when drawn;
	 */
	public Island(Site startSite, int id){
		this.ID = id;
		this.startSite = startSite;

		landMass  = new HashMap<String, Site>();
		toExplore = new ArrayList<Site>();
		Collections.synchronizedMap(landMass);

		addSite(startSite);

	}

	public void addSite(Site a){
		if(!landMass.containsKey(a.locationString())){
			landMass.put(a.locationString(),a);
			toExplore.add(a);
		}
	}

	public boolean allExplored(){
		if(toExplore.size() == 0){
			return true;
		}else{
			return false;
		}

	}

	public void explore(){
		ArrayList<Site> toAdd = new ArrayList<Site>();
		ArrayList<Site> explored = new ArrayList<Site>();
		//While there are still unexplored sites ...
		while(!allExplored()){
			//Go through each of the unexplored sites ...
			for (Site s : toExplore) {
				//And add each of its neighbours to a temp list
				for (Site neighbour : s.getNeighbours()) {
					toAdd.add(neighbour);
				}
				//This had now been explored so we will add it to another list which will remove in a minute
				explored.add(s);
			}
			//Now add these neighbours to the islands
			for(Site toAddSite : toAdd){
				addSite(toAddSite);
			}
			//Clean up
			toAdd.clear();
			//Remove the sites we have explored
			toExplore.removeAll(explored);
			explored.clear();
		}
	}

	/**
	 * Test to see if and random site in Island j's landMass is in this Islands landMass.
	 * @param j The test island
	 * @return True if any site is in both landMasses
	 */
	public boolean sameIsland(Island j){
		return landMass.containsKey(j.startSite.locationString());
	}

	/**
	 * Randomly add a civilisation with given index based on the carrying capacity of the grid. 
	 * @param index of the civilisation to add 
	 */
	public void randomAddCivilisation(int index, int numberOfCivs){
		for(Site site : landMass.values()){
			site.addPopulation(index, Cartographer.randomDouble(0.0, site.getK()/(2.0*(double)numberOfCivs)));
			//site.addPopulation(index, site.getK()/(double)numberOfCivs);
		}
	}
	
	/**
	 * The total population on the island
	 * @return population on the island
	 */
	public double totalPopulation() {
		double output = 0.0;
		for(Site site : landMass.values()){
			output += site.getTotal();
		}
		return output;

	}

	public void draw(Graphics2D g, int xOffset, int yOffset, int h){
		g.setColor(colours[ID%colours.length]);
		for (Site site : landMass.values()) {
			site.draw(g, xOffset, -yOffset, h);
		}
	}

	@Override
	public String toString() {
		return ID+"";
	}
}

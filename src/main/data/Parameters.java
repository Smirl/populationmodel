package main.data;

import java.io.Serializable;
import java.util.HashMap;


/**
 * An object encapsulating a hash map of string double pairs of contants for use in the model. 
 * @author s0941897
 */
public class Parameters implements Serializable{

	private static final long serialVersionUID = 8159739470564308483L;
	/**
	 * The map of parameters
	 */
	private HashMap<String, Double> params;
	
	/**
	 * Create an empty parameter object
	 */
	public Parameters() {
		params = new HashMap<String, Double>();
	}
	
	/**
	 * Create an filled parameter object with the key value pairs given
	 * @param keyValues Space separated key value pairs.
	 */
	public Parameters(String keyValues){
		this();
		addPairs(keyValues);
		
		System.out.println(this);
	}
	
	/**
	 * Add the space separated String-Double pairs into the parameter list.
	 * @param keyValues Space separated key value pairs.
	 */
	public void addPairs(String keyValues){
		
		String[] pairs = keyValues.split(" ");
		//Check that the keyValues are in the correct format
		if(pairs.length==0){
			throw new IllegalArgumentException("Argument string must be space separated");
		}else if(pairs.length%2!=0){
			throw new IllegalArgumentException("Argument must have space separated key-value pairs");
		}
		
		//Loop over the split string and put the values into the hashmap
		for (int i = 0; i < pairs.length; i+=2) {
			params.put(pairs[i], Double.parseDouble(pairs[i+1]));
		}
	}
	
	/**
	 * Clear all of the pairs from the parameter list
	 */
	public void clear(){
		params.clear();
	}
	
	/**
	 * Get the parameter with the given key
	 * @param key The string which represents the parameter
	 * @return The desired parameter
	 * @see HashMap
	 */
	public double get(String key){
		return params.get(key);
	}
	
	/**
	 * Set a particular key to a new value or add it if it is not in the list
	 * @param key String name of parameter
	 * @param value The double value of the parameter
	 * @see HashMap
	 */
	public void set(String key, double value){
		params.put(key, value);
		System.out.println(key+" =  "+value);
	}
	
	@Override
	public String toString() {
		return "Parameters: "+params.toString();
	}
	

	
}

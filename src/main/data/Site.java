package main.data;
import java.awt.Graphics2D;
import java.io.Serializable;
import java.util.ArrayList;


/**
 * @author s094189
 * The grid site for a population model. Contains position, carrying capacity, population of all civilisations and list of neighbour sites.
 */
public class Site implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/* *************************** THE VARIABLES ****************************/
	/**
	 * The nearest neighbours to this current site. 0=up, 1=right, 2=down and 3=left. 
	 */
	private Site[] neighbours;

	/**
	 * The x position of the site
	 */
	private int x;

	/**
	 * The y position of the site
	 */
	private int y;

	/**
	 * Carrying capacity (saturation population of site
	 */
	private double k;

	/**
	 * The natural carrying capacity of the site
	 */
	private double k0;
	
	/**
	 * Population of different civilisations on this site
	 */
	private ArrayList<Density> n = new ArrayList<Density>();
	


	/* *************************** THE CONSTUCTORS ****************************/
	/**
	 * Default site constructor setting one civilization with a population of 0. The location is (0,0) and k=0;
	 */
	public Site() {
		this(0,0,0.0,null);
	}

	/**
	 * A new site at location (i,j) with 0 population and k=0
	 * @param i x pos
	 * @param j y pos
	 */
	public Site(int i, int j){
		this(i,j,0.0,null);
	}

	/**
	 * Sets the position and carrying capacity of the site
	 */
	public Site(int i, int j,double k) {
		this(i,j,k,null);
	}

	/**
	 * Sets the position, the carrying capacity and populations
	 * @param a
	 */
	public Site(int i, int j, double k, ArrayList<Density> n){
		this.x=i;
		this.y = j;
		this.k = k;
		this.k0 = k;
		if(n==null)
			this.n.add(new Density());
		else
			this.n.addAll(n);
	}

	/**
	 * Complete copy of a site Do not use if copying to new grid
	 * @see copyWithoutNeighbour
	 * @param a The site to copy
	 */
	public Site(Site a){
		x=a.x;
		y=a.y;
		k=a.k;
		neighbours = a.neighbours.clone();
		n.addAll(a.n);
	}

	/* *************************** THE GETTERS ****************************/
	/**
	 * @param dir the int of the direction of neighbour required
	 * @return the neighbour in dir direction
	 */
	public Site getNeighbour(int dir) {
		return neighbours[dir];
	}

	/**
	 * Get the array of neighbours
	 * @return neighbour array
	 */
	public Site[] getNeighbours(){
		return neighbours;
	}
	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @return the k
	 */
	public double getK() {
		return k;
	}
	
	/**
	 * Return the natural k
	 * @return k0
	 */
	public double getK0(){
		return k0;
	}
	
	/**
	 * The list of populations
	 * @return ArrayList of population densities on this site
	 */
	public ArrayList<Density> getPopulation(){
		return n;
	}

	/**
	 * @param civ the number of civilisation who's population at this site is returned
	 * @return the population of civ at this site
	 */
	public double getPopulation(int civ) {
		return n.get(civ).get();
	}
	
	public int size(){
		return n.size();
	}

	/**
	 * Sums up the total number of people, etc. on a grid site from all civilisatons
	 * @return The total population on the site
	 */
	public double getTotal(){
		double output = 0.0;
		for (int i = 0; i < n.size(); i++) {
			output+=n.get(i).get();
		}
		return output;
	}


	/* *************************** THE SETTERS ****************************/
	
	/**
	 * @param neighbours the neighbours to set
	 */
	public void setNeighbours(Site[] neighbours) {
		this.neighbours = neighbours;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Set both the x and y coordinates
	 * @param x
	 * @param y
	 */
	public void setLocation(int x, int y){
		this.x = x;
		this.y = y;
	}

	/**
	 * @param k the k to set
	 */
	public void setK(double k) {
		this.k = k;
	}

	/**
	 * Set the natural value of the carrying capacity to the value given
	 * @param k0
	 */
	public void setK0(double k0){
		this.k0 = k0;
	}
	
	/**
	 * Set a value of a civ explictially. Not normall used. Use addPopulation.
	 * @param civ The index of the civilisation
	 * @param val The new value it should have
	 * @see addPopulation(int, double)
	 */
	public void setPopulation(int civ, double val){
		this.n.get(civ).set(val);
	}
	
	/**
	 * Replace the density list with a's in a nice way (i.e. not pointing)
	 * @param a The site of "copy"
	 */
	public void update(Site a){
		//Update the densities
		for (int i = 0; i < a.n.size(); i++) {
			if(i<this.n.size())
				this.n.get(i).set(a.n.get(i).get());
			else
				this.n.add(new Density(a.n.get(i).get()));
		}
		
		//Update the K
		this.k = a.k;
		
	}


	/* *************************** THE METHODS ****************************/

	/**
	 * Add the ammout, dk, to the value of K on this site
	 */
	public void addK(double dk){
		this.k += dk;
		if(this.k < 1.0)
			this.k = 1.0;
	}

	/**
	 * Adds dn to the civilisation with index civ. If civ does not exist, then all previous civs are set to 0 and the final set to dn.
	 * @param civ Index of civilisation
	 * @param dn Change to the population
	 */
	public void addPopulation(int civ, double dn){

		//if number of populations is greater than the given index (civ) then it must exist so set this new value +dn
		if(n.size() > civ){

			//first calculate the new population
			double diff = n.get(civ).get() + dn;

			//Do NOT allow negative populations (allows for some Double precision errors)
			if(diff > 0){
				n.get(civ).set(n.get(civ).get() + dn);
			}
			else{
				n.get(civ).set(0.0);
			}
		}

		//if number of populations is less than civ then (because the way array lists work) we need to add the other populations (set to 0) 
		if(n.size() <= civ){			

			//add intermediate populations
			int numIterations = civ - n.size();
			for(int i=0; i < numIterations; i++){

				n.add(new Density());
			}

			//add the given population; check that it is not negative
			if(dn > 0) n.add( new Density(dn));
			else n.add( new Density());			
		}		
	}

	/**
	 * Draws the site to the Graphics@D Object g. Other variables needed to corrctly position the site;
	 * @param g
	 * @param xOffset
	 * @param yOffset
	 * @param height
	 */
	public void draw(Graphics2D g, int xOffset, int yOffset, int height){
		g.fillRect(x - xOffset, height - y - yOffset, 1, 1);
	}

	/**
	 * See if a is in the same location as this
	 * @param a the test Site
	 * @return If on the same location
	 */
	public boolean sameLocation(Site a){
		if(this.x == a.x && this.y == a.y){
			return true;
		}else{
			return false;
		}
	}

	public boolean hasNeighbour(Site a){
		boolean[] directions = new boolean[4];

		directions[0] = (this.x-1 == a.x && this.y   == a.y);
		directions[1] = (this.x+1 == a.x && this.y   == a.y);
		directions[2] = (this.x == a.x   && this.y-1 == a.y);
		directions[3] = (this.x == a.x   && this.y+1 == a.y);

		return (directions[0] || directions[1] || directions[2] || directions[3]);

	}

	/**
	 * Return a copy of the site without a copy of the neighbour site array. This is to be set with cartographer.findNieghbours
	 * @param a
	 * @return the partly copied site
	 */
	public Site copyWithoutNeighbour(){
		return new Site(x,y,k,n);
	}

	@Override
	public int hashCode() {
		return x*y;
	}

	public String locationString(){
		return x+" "+y;
	}

	@Override
	public boolean equals(Object obj){
		return (super.equals(obj) || sameLocation((Site)obj) );
	}

}

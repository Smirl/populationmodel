/**
 * 
 */
package main.runnables;

import main.data.Civilisation;

/**
 * @author s0941897
 *
 */
public class CivilisationUpdater implements Runnable {

	Civilisation civilisation;
	
	/**
	 * 
	 */
	public CivilisationUpdater(Civilisation c) {
		this.civilisation = c;
	}

	/**
	 * Updates the civilisation
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		//Updates the total population and whether or not to kill the civiilisation
		if(!this.civilisation.isDead())
			this.civilisation.update();
	}

}

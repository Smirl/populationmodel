package main.runnables;

import main.Model;

public class SectionCopier implements Runnable{

	private int start;
	private int end;
	
	public SectionCopier(int start, int end){
		this.start = start;
		this.end = end;
	}
	
	@Override
	public void run() {
		Model.model.updateWorkingMap(this.start,this.end);
	}

}

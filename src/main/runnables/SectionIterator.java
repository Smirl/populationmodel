/**
 * 
 */
package main.runnables;

import main.Model;

/**
 * Called to run iterate of a Model on a section of it's map
 * 
 * @author s0941897
 *
 */
public class SectionIterator implements Runnable {

	private int startIndex; 
	private int endIndex;

	/**
	 * 
	 */
	public SectionIterator(int start, int end) {
		startIndex=start;
		endIndex =  end;
	}

	/** 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		for (int i = startIndex; i < endIndex; i++) {
			Model.model.iterate(i);
		}
	}

}

/**
 * 
 */
package run;

import viewer.InteractivePopulation;
import main.Cartographer;
import main.Output;
import main.PopulationModel;

/**
 * @author s0941897
 *
 */
public class IOTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		testHumanIO();
	}
	
	@SuppressWarnings({ "serial" })
	private static void testHumanIO(){
		final PopulationModel fileModel = new PopulationModel("../../Desktop/mapdata/uk.dat", null, null);
		
		new InteractivePopulation(){
			protected void createModel() {
				m = fileModel;
			};
		};
	}
	
	@SuppressWarnings("unused")
	private static void testBinaryIO(){
		PopulationModel m1 = new PopulationModel(Cartographer.generateSquare(0, 100, 2, false));

		Output.modelToFile(m1, "testMap.in");

		PopulationModel m2 = new PopulationModel("testMap.in");

		boolean same = true;
		for (int i = 0; i < m1.getMap().length; i++) {
			if(!m1.getMap()[i].sameLocation(m2.getMap()[i])){
				System.out.println("Not the same map");
				same=false;
			}
		}
		System.out.println("Same map? "+same);
		System.out.println();

		System.out.println(m1.getParameters());
		System.out.println(m2.getParameters());
		System.out.println();

		for (int i = 0; i < m1.getCivilisations().size(); i++) {
			System.out.println(m1.getCivilisations().get(i));
			System.out.println(m2.getCivilisations().get(i));
		}
		System.out.println();
		
		System.out.println(m1.getMatrix());
		System.out.println(m2.getMatrix());
		System.out.println();
	}

}

package run;

import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import viewer.GridViewer;
import viewer.InteractivePopulation;
import main.Cartographer;
import main.PopulationModel;
import main.data.Civilisation;
import main.data.Density;
import main.data.Site;

/**
 * Used to run a cut back version of the {@link PopulationModel} genrally on a single site.
 * If you are running on more than one site then set the number of threads higher. 
 * @author s0941897
 *
 */
public class ModelAbstractionTest {

	private static PrintWriter out; 

	static int iterations = 0;
	static int printevery;
	static int civs;
	static int length = 0;
	static int numberOfThreads = 1;
	static double startk;
	static double startk0;
	static boolean half = true;
	static boolean quiet = false;
	static boolean[] method = new boolean[]{false,false,false,false};
	static ArrayList<Density> startp = new ArrayList<Density>();
	static ArrayList<String> param = new ArrayList<String>();
	static ArrayList<Civilisation> putcivs = new ArrayList<Civilisation>();
	static Cartographer mapMaker;
	static PopulationModel model;
	static String filename = "basicDefaults.dat";

	public static void main(String[] args) throws IOException, HeadlessException {

		parseArgs(args);

		//Check if someone is trying to run the grid half without two civs
		if(civs!=2 && length!=0){
			System.err.println("Trying to run a 2d map on 50-50 mode without exactly two civilisations." +
			"\nSet the correct number of civilisations or run with -equal for equal spread of civs.");
			System.exit(1);
		}

		out = null;
		try {
			out = new PrintWriter(new File(filename));
		} catch (Exception e) {
		}

		if(iterations == 0)
			printevery = 10;
		else
			printevery = iterations/100;

		System.out.println(printevery);

		mapMaker = new Cartographer(){
			private void addSingle(){
				Site putSite = new Site(xOffset, yOffset, startk, startp);
				putSite.setNeighbours(new Site[0]);
				putSite.setK0(startk0);
				cartWorkingMap.put(putSite.locationString(), putSite);
			}
			private void addLand(){
				for (int i = 0; i < length; i++) {
					for (int j = 0; j < length; j++) {
						Site putsite = new Site(GridViewer.xOffset + i, -GridViewer.yOffset + j, startk);
						cartWorkingMap.put(putsite.locationString(), putsite);
					}
				}
				findNeighbours(false);
			}
			private void half(){
				for (Site site : cartWorkingMap.values()) {
					if(site.getX()<GridViewer.xOffset + length/2){
						site.addPopulation(0, startp.get(0).get());
						site.addPopulation(1, 0.0);
					}else{
						site.addPopulation(0, 0.0);
						site.addPopulation(1, startp.get(1).get());
					}
				}
			}
			public void equal(){
				for (Site site : cartWorkingMap.values())
					for (int j = 0; j < cartCivilisations.size(); j++) 
						site.addPopulation(j, startp.get(j).get());
			}

			private void addCivs(){
				cartCivilisations.addAll(putcivs);
			}
			public void generate() {
				clearLand();
				clearK(0.0);
				clearCivilisations();
				addCivs();

				if(length==0){
					addSingle();
				}else{
					addLand();
					if(half)
						half();
					else
						equal();
				}
			}
		};

		mapMaker.generate();

		model = new PopulationModel(numberOfThreads, mapMaker){
			protected void preIterate() {
				super.preIterate();
				if(!quiet)
					System.out.println(steps+" "+totalK()+" "+printPop());//Arrays.toString(subTotalPopulation()));

				if(steps%10==0){
					out.println(steps+" "+totalK()+" "+printPop());
					out.flush();
				}
			};
			public void iterate(int i){
				//if(method[0])
				//	competitiveLV(i);
				//if(method[1])
				//	nonLogistic(i);
				//if(method[3])
				birthdeathcompetition(i);
				//if(method[2])
				updateK(i);
			}
			protected void postIterate() {
				//Do nothing
			};
			private String printPop(){
				String out ="";
				double[] tots = subTotalPopulation();
				for (int j = 0; j < tots.length; j++) {
					out += tots[j] + " ";					       }
				return out;
			}
		};

		for(int i =0; i< param.size(); i++){
			String[] kv = param.get(i).split("=");
			model.getParameters().set(kv[0], Double.parseDouble(kv[1]));
		}
		
		
		if(iterations==0){
			new InteractivePopulation(){
				private static final long serialVersionUID = 1L;
				protected void createModel() {
					m = model;
					System.out.println("K  : "+m.totalK()+"\nPop: "+Arrays.toString(m.subTotalPopulation())+"\n"+printCivs(m.getCivilisations()));
				}
			};
		}else{
			for(int i = 0; i<iterations; i++){
				model.simulationStep();
				
				//If everyone is dead then stop. 
				if(model.totalPopulation() == 0){
					System.out.println("Stopped at "+i+" steps as total population is 0");
					break;
				}
			}
			System.out.println("Done");
			out.close();
			System.exit(0);
		}

		out.close();
	}


	private static String printCivs(ArrayList<Civilisation> civs){
		String out = "";
		for (int i = 0; i < civs.size(); i++) {
			out += civs.get(i).toString() + "\n";
		}

		return out; 
	}

	/**
	 * Used to parse the command line arguments. In a similar style to {@link PMPopVTime}
	 * @param args
	 */
	private static void parseArgs(String[] args){
		for (int i = 0; i < args.length; i++) {
			if(args[i].equals("-f")){
				filename = (args[i+1]);
			}
			if(args[i].equals("-k")){
				startk = Double.parseDouble(args[i+1]);
				startk0=startk;
			}
			if(args[i].equals("-k0")){
				startk0= Double.parseDouble(args[i+1]);
			}
			if(args[i].equals("-c")){
				civs = Integer.parseInt(args[i+1]);
			}
			if(args[i].equals("-T")){
				iterations =Integer.parseInt(args[i+1]);
			}
			if(args[i].equals("-t")){
				numberOfThreads =Integer.parseInt(args[i+1]);
			}
			if(args[i].equals("-l")){
				length =Integer.parseInt(args[i+1]);
			}
			if(args[i].equals("-q")){
				quiet=true;
			}
			if(args[i].equals("-param")){
				param.add(args[i+1]);
			}
			if(args[i].equals("-p")){
				i++;
				while(i<args.length && !args[i].contains("-")){
					startp.add( new Density(Double.parseDouble(args[i])));
					System.out.println("StartP: "+startp);
					i++;
				}
				i--;
				continue;
			}
			if(args[i].equals("-S")){
				putcivs.clear();
				i++;
				for (int j = 0; j < civs; j++) {
					putcivs.add(new Civilisation(Double.parseDouble(args[i]),Double.parseDouble(args[i+1]),Double.parseDouble(args[i+2])));
					i += 3;
				}
				i -= civs * 3 - 1;
				continue;
			}
			if(args[i].equals("-lv")){
				method[0]=true;
			}
			if(args[i].equals("-nonlv")){
				method[1]=true;
			}
			if(args[i].equals("-updatek")){
				method[2]=true;
			}
			if(args[i].equals("-bdc")){
				method[3]=true;
			}
			if(args[i].equals("-equal")){
				half=false;
			}
		}
	}


}


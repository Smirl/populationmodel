package run;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

import main.Cartographer;
import main.Output;
import main.PopulationModel;
import main.data.Civilisation;

public class PMPopVTime {

	
	private static String fileName = "output/PopVTime/popVtime.dat";
	private static double density = 0.2;
	private static int threads = 32;
	private static int seed = 100;
	private static int civs = 3;
	private static int steps = 100;
	private static int type = 0;
	private static double longestSide = 100.0;
	private static boolean flatk=false;
	private static boolean binary=true;
	private static ArrayList<String> param = new ArrayList<String>();

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		//Parse the input arguments 
		parseArguements(args);

		//Create the printwriter
		PrintWriter out = null;
		try {
			out = new PrintWriter(new File(fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Create the model
		PopulationModel m = null;
		if(type==0){
			m = new PopulationModel(Cartographer.generateSquare(seed,longestSide,civs,flatk));
			System.out.println("\nSquare. Seed="+seed+" Length="+longestSide+" CivNum="+civs+" Timesteps: "+steps+"\n");
		}else if (type==1){
			m = new PopulationModel(Cartographer.generateIslands(seed, density, 5, longestSide, civs));
			System.out.println("\nIslands. Seed="+seed+" Length="+longestSide+" CivNum="+civs+" Timesteps: "+steps+" Density="+density+"\n");
		}else if(type==2){
			m = new PopulationModel(Cartographer.generateArena(seed, (int) longestSide, flatk));
			System.out.println("\nArena. Seed="+seed+" Length="+longestSide+" CivNum="+civs+" Timesteps: "+steps+"\n");
		}else if(type==3){
			Cartographer.r = new Random(seed);
			m = new PopulationModel("../../../Desktop/mapdata/uk.dat", null, null);
			System.out.println("\nUK. Seed="+seed+" CivNum="+1+" Timesteps: "+steps+"\n");
		}
		m.setNumberOfThreads(threads);
		//Set the parameter options
		for(int i=0; i<param.size(); i++){
			String[] kv = param.get(i).split("=");
			m.getParameters().set(kv[0],Double.parseDouble(kv[1]));
		}

		//How often to print output
		int printEvery = 1;
		if(steps>200)
			printEvery = steps/200;

		//print first point
/*		double initialTotal = 0.0;
		for (int j = 0; j < m.getCivilisations().size(); j++) {
			initialTotal += m.getCivilisations().get(j).calculateTotalPopulation();
		}
		out.println(0+" "+String.format("%10.5f",initialTotal)+" "+printArray(m.subTotalPopulation()));
		out.flush();*/


		//Run the simulation
		long startTime = System.currentTimeMillis();
		for (int i = 1; i < steps; i++) {
			m.simulationStep();
			//Print output
			if(i%printEvery ==0) {
				out.println(i+" "+String.format("%10.5f",m.totalK())+" "+printArray(m.subTotalPopulation()));
				out.flush();
			}
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Time to complete: "+(endTime-startTime));
		out.close();


		//Print the civilisations out to a file
		try {
			out = new PrintWriter(new File(fileName+".civs"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(printCivs(m.getCivilisations()));
		out.println(printCivs(m.getCivilisations()));
		out.close();

		if(binary)
			Output.modelToFile(m, fileName+".model");
		
		System.out.println(String.format("%10.5f",m.totalK())+" "+printArray(m.subTotalPopulation()));

		System.exit(0);
	}

	private static String printArray(double... a){
		String out = "";

		for (int i = 0; i < a.length; i++) {
			out += String.format("%10.5f",a[i]) + " ";
		}

		return out;
	}

	/**
	 * Returns a string with the civilisations listed
	 * @param civs The list of civilisations
	 * @return The formatted string with line spaces
	 */
	private static String printCivs(ArrayList<Civilisation> civs){
		String out = "";
		for (int i = 0; i < civs.size(); i++) {
			out += civs.get(i).toString() + "\n";
		}
		return out; 
	}

	/**
	 * Set up the variables from the input arguments
	 * @param args
	 */
	private static void parseArguements(String[] args){
		for (int i = 0; i < args.length; i++) {
			if(args[i].equals("-f")){
				fileName = args[i+1];
			}
			if(args[i].equals("-d")){
				density = Double.parseDouble(args[i+1]);
			}
			if(args[i].equals("-t")){
				threads = Integer.parseInt(args[i+1]);
			}
			if(args[i].equals("-s")){
				seed = Integer.parseInt(args[i+1]);
			}
			if(args[i].equals("-c")){
				civs = Integer.parseInt(args[i+1]);
			}
			if(args[i].equals("-T")){
				steps = Integer.parseInt(args[i+1]);
			}
			if(args[i].equals("-l")){
				longestSide = Double.parseDouble(args[i+1]);
			}
			if(args[i].equals("-b")){
                               	binary=false;
                        }
			if(args[i].equals("-square")){
				type=0;
			}
			if(args[i].equals("-islands")){
				type=1;
			}
			if(args[i].equals("-arena")){
				type=2;
			}
			if(args[i].equals("-uk")){
				type=3;
			}
			if(args[i].equals("-z")){
				flatk = true;
			}
			if(args[i].equals("-p")){
				param.add(args[i+1]);
			}
			if(args[i].equals("-man")){
				System.out.println("-f:\t (String) File name for the output file");
				System.out.println("-d:\t (double) Island density of land for Cartographer. default=0.2");
				System.out.println("-t:\t (int) Number of threads. default=32");
				System.out.println("-s:\t (int) Seed number. default=100");
				System.out.println("-c:\t (int) Number of civilisations. default=3");
				System.out.println("-T:\t (int) Number of timesteps. default=100");
				System.out.println("-l:\t (double) Length of square containing land. default=100.0");
				System.out.println("-square: If on a square map. default=false");
				System.out.println("-islands: If on an island map. default=true");
				System.exit(0);
			}
		}
	}
}

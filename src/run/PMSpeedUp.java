package run;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import main.Cartographer;
import main.PopulationModel;

/**
 * 
 */

/**
 * This runs the PopulationModel for 4 given densities for single thread, and then 2,4,8,16 threads parallel. 
 * The output file "speedup.dat" gives the "density threads timeToComplete ans". The number of steps is 100<br>
 * <br>
 * Use -f arg to set teh file name. -d arg0 arg1 ... to set densities. -t arg0 arg1 ... to set threads. 
 * @author s0941897
 *
 */
@SuppressWarnings("unused")
public class PMSpeedUp {

	//227953 0.2 100 500.0

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Defaults
		String fileName = "speedup.dat";
		Double[] defaultTestLengths = {100.0};
		Double[] defaultTestDensities = {0.2, 0.4, 0.6, 0.8};
		Integer[] defaultTestThreads = {1,2,4,8,16,32,64,100};	

		//Things to use
		boolean square = false;
		int seed = 100;
		int civs = 3;
		int steps = 1000;
		ArrayList<Double> testLengths = new ArrayList<Double>();
		ArrayList<Double> testDensities = new ArrayList<Double>();
		ArrayList<Integer> testThreads = new ArrayList<Integer>();

		//Add the defaults
		testLengths.addAll(Arrays.asList(defaultTestLengths));
		testDensities.addAll(Arrays.asList(defaultTestDensities));
		testThreads.addAll(Arrays.asList(defaultTestThreads));

		//Command line arguments flags
		for (int i = 0; i < args.length; i++) {
			if(args[i].equals("-f")){
				fileName = args[i+1];
				i++;
			}
			if(args[i].equals("-d")){
				testDensities.clear();
				i++;
				while(i<args.length && !args[i].contains("-")){
					testDensities.add(Double.parseDouble(args[i]));
					i++;
				}
				i--;
				continue;
			}
			if(args[i].equals("-t")){
				testThreads.clear();
				i++;
				while(i<args.length && !args[i].contains("-")){
					testThreads.add(Integer.parseInt(args[i]));
					i++;
				}
				i--;
				continue;
			}
			if(args[i].equals("-l")){
				testLengths.clear();
				i++;
				while(i<args.length && !args[i].contains("-")){
					testLengths.add(Double.parseDouble(args[i]));
					i++;
				}
				i--;
				continue;
			}
			if(args[i].equals("-s")){
				seed = Integer.parseInt(args[i+1]);
			}
			if(args[i].equals("-c")){
				civs = Integer.parseInt(args[i+1]);
			}
			if(args[i].equals("-T")){
				steps = Integer.parseInt(args[i+1]);
			}
			if(args[i].equals("-square")){
				square=true;
				testDensities.clear();
				testDensities.add(0.0);
			}
			if(args[i].equals("-islands")){
				square=false;
			}
			if(args[i].equals("-man") || args[i].equals("-help")){
				System.out.println("-f:\t (String) File name for the output file\n");

				System.out.println("The following three can be multiple values, space seperated");
				System.out.println("-l:\t (double) Length of square containing land. default=100.0");
				System.out.println("-d:\t (double) Island density of land for Cartographer. default=0.2");
				System.out.println("-t:\t (int) Number of threads. default=32\n");

				System.out.println("-s:\t (int) Seed number. default=100");
				System.out.println("-c:\t (int) Number of civilisations. default=3");
				System.out.println("-T:\t (int) Number of timesteps. default=100");
				System.out.println("-square: If on a square map. default=false");
				System.out.println("-islands: If on an island map. default=true");
				System.exit(0);
			}
		}
		System.out.println("File Name: "+fileName);
		System.out.println("Seed: "+seed+". Square: "+square+". Steps: "+steps);
		System.out.println("Lengths: "+Arrays.toString(testLengths.toArray()));
		System.out.println("Densities: "+Arrays.toString(testDensities.toArray()));
		System.out.println("Threads:   "+Arrays.toString(testThreads.toArray())+"\n");

		PrintWriter out = null;
		try {
			out = new PrintWriter(new File(fileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}


		PopulationModel p;

		//The number of steps and the tenPercent for printing the current progress
		int tenPercent = steps/10;

		out.println("Seed: "+seed+" Square: "+square+" Steps: "+steps);

		for(double len : testLengths){
			//For all densities
			for(double den : testDensities){
				//For the number of threads
				for(int n : testThreads){

					//Make a new model and set threads
					if(!square)
						p = new PopulationModel(Cartographer.generateIslands(seed, den, 5, len, civs));
					else
						p = new PopulationModel(Cartographer.generateSquare(seed, len, civs, false));
					p.setNumberOfThreads(n);

					//The all important start time
					long startTime;

					System.out.println("\nSimulation Started on "+n+" threads. Land density: "+den);
					//If only one thread run the non parallel version of the iterate
					if(n==1){
						startTime = System.currentTimeMillis();
						for(int t=0;t<steps;t++){
							p.iterateAll();
							progress(t,tenPercent);
						}
						//Else run the parallel one
					}else{
						startTime = System.currentTimeMillis();
						for(int t=0;t<steps;t++){
							p.simulationStep();
							progress(t,tenPercent);
						}
					}
					//Print stuff out and flush it in case the world ends and i can look at some output in hell
					long endTime = System.currentTimeMillis();
					System.out.println("\nTotal Population: "+p.totalPopulation());
					System.out.println("Time to complete: "+(endTime-startTime));

					out.println(len+" "+den+" "+n+" "+(endTime-startTime)+" "+p.totalPopulation());
					out.flush();

					p.clean();
					System.gc();
				}
			}
			out.println();
		}
		//Close the Writer and stuff
		out.close();
		System.exit(0);
	}

	private static void progress(int t, int tenPercent){
		if(t%tenPercent == 0){
			System.out.print(">");
		}
	}
}


/**
 * 
 */
package run;

import main.Cartographer;
import main.PopulationModel;
import main.data.Civilisation;

/**
 * @author s0941897
 *
 */
public class RandomCivTest {
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Cartographer();

		
		
		PopulationModel m = new PopulationModel(Cartographer.generateSquare(0, 100.0, 2, false)){
			int[] type = new int[]{0,0,0};
			@Override
			protected void preIterate() {
				System.out.println(type[0] +" " +type[1] +" "+type[2]);
			}
			@Override
			public void iterate(int i) {
			}
			@Override
			protected void postIterate() {
				Civilisation newciv = new Civilisation();
				civilisations.add(newciv);
				type[getType(newciv)]++;
				
			}
			
			private int getType(Civilisation c){
				if(c.getBirthrate() > c.getIntellect())
					if(c.getBirthrate() > c.getAggression())
						return 0;
					else
						return 2;
				else if(c.getIntellect() > c.getAggression())
					return 1;
				else return 2;
			}
		};
		
		for (int i = 0; i < 1000; i++) {
			m.simulationStep();
		}
		
		
	}

}

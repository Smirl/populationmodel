/**
 * 
 */
package run;



/**
 * @author s0941897
 *
 */
public class RandomLogTest {
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		double x = 50.0;
		double k = 100.0;
		double r = 0.02;
		double d = 0.02;
		
		for (int i = 0; i < 1000; i++) {
			System.out.println(i+" "+x);
			x = x + 0.01*r*x*( 1 - (x/k)) - d*x;
			if(x<1.0)
				return;
		}
		
	}

}

package run;

import java.util.ArrayList;
import java.util.Arrays;

import main.Cartographer;
import main.PopulationModel;
import main.data.Civilisation;
import main.data.Site;

public class RunNoGUI {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		int steps;
		
		if(args.length==0)
			steps=100;
		else
			steps=Integer.parseInt(args[0]);
		
		Cartographer mapMaker = new Cartographer(){
			private void addLand(){
				Site putSite = new Site(xOffset, yOffset, 25.0, new ArrayList<Double>(Arrays.asList(new Double[]{25.0})));
				putSite.setNeighbours(new Site[0]);
				cartWorkingMap.put(putSite.locationString(), putSite);
			}
			private void addCivs(){
				cartCivilisations.add(new Civilisation(0.0, 1.0, 0.0));
				//cartCivilisations.add(new Civilisation(0.1, 0.9, 0.0));
			}
			public void generate() {
				clearLand();
				clearK(0.0);
				clearCivilisations();
				addLand();
				addCivs();
			}
		};
		
		mapMaker.generate();
		
		PopulationModel model = new PopulationModel(mapMaker);		
		
		for (int i = 0; i < steps; i++) {
			model.simulationStep();			
		}
		
	}

}

/**
 * 
 */
package run;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.JFrame;
import javax.swing.JPanel;

import viewer.GridViewer;
import main.data.Civilisation;
import main.data.Site;

/**
 * @author Alex
 *
 */
public class ShowBinaryMap {
	
	static Site[] map = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String fileName= "1000-2-5-500.map";
		
		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(new FileInputStream(fileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Civilisation[] civilisations = new Civilisation[3];
		for (int i = 0; i < civilisations.length; i++) {
			civilisations[i] = new Civilisation();
		}
		
		try {
			map = (Site[]) in.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		if(map==null){
			throw new NullPointerException("Map has not been read properly");
		}
		
		JFrame frame = new JFrame(fileName);
		
		JPanel pane = new JPanel(){
			private static final long serialVersionUID = 1L;

			protected void paintComponent(java.awt.Graphics arg0) {
				
				for(Site site : map){
					site.draw((Graphics2D)getGraphics(), GridViewer.xOffset, GridViewer.yOffset, getHeight());
				}
				
			};
		};
		
		pane.setPreferredSize(new Dimension(400,400));
		
		frame.add(pane);
		
		frame.pack();
		frame.setVisible(true);
	}

}

/**
 * 
 */
package run;

import viewer.InteractivePopulation;

import main.PopulationModel;

/**
 * @author s0941897
 *
 */
public class ViewModel {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length < 1){
			System.err.println("No arguments given. First should be the model file");
			System.exit(1);
		}
		
		final PopulationModel model = new PopulationModel(args[0]);
		
		new InteractivePopulation(){
			private static final long serialVersionUID = 1L;

			protected void createModel() {
				m = model;
			};
		};
	}

}

package viewer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import main.data.Site;

/**
 * The GridViewer which displays all n species on top of one another using
 * AlphaComposites. The Colours are calculated in the same way as Single Viewer
 * to aviod confustion.
 */
public class AllViewer extends GridViewer {

	private static final long serialVersionUID = 8882444643457387242L;
	
	/**
	 * Create a viewer and attach it to the controller
	 * 
	 * @param attachedTo
	 */
	public AllViewer(InteractivePopulation attachedTo) {
		super(attachedTo);
	}
	

	/**
	 * Sets the fast rendering options
	 */
	private void setFastRender() {
		Graphics2D g = (Graphics2D) getGraphics();

		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
		g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
		g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
		g.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
	}

	@Override
	public void initialise() {
		super.initialise();
		setFastRender();
	}

	/**
	 * The Paint method. This is where most of the work happens with getting the
	 * map, calculating map sizes and painting. The gridwidth calculation could
	 * be done on resize of the frame to avoid computation but this is not too
	 * much of a problem on small maps.
	 * 
	 * @param gg
	 *            the Graphics component on which to paint
	 */
	protected void paintComponent(Graphics gg) {
		
		super.paintComponent(gg);
		
		Graphics2D g = (Graphics2D) gg;

		// Clear the background if we are an opaque component
		g.setColor(getBackground());
		g.fillRect(0, 0, getWidth(), getHeight());
		
		g.setColor(Color.PINK);
		g.drawString("Maximum Density Visible (Drawscale): " + maxScale + " xOffset: "+xOffset+ " yOffset: "+yOffset, 20, 20);

		//Set scale
		g.scale(scale, scale);

		// Loop over the grid and if the total population on a site is bigger
		// than the current drawscale, make this is max.
		// Is used so that all alpha is scaled between the max and zero
		// This is now called in the synconized getCurrentGrid();

		// If the slider has change from 100% in the user panel, then
		// DRAWSCALE_CHANGED is true.
		// Therefore if it is at 100% the drawScale is set to the maximum, which
		// can change over time.

		if (!DRAWSCALE_CHANGED) {
			drawScale = maxScale;
			getController().setUserSliderLabel(maxScale);
		} else {
			getController().setUserSliderScale(maxScale);
		}

		double inverseDrawscale = 1.0/getDrawScale();
		
		// Paint a rectangle for each site with the colour defined above if it
		// is
		// less than the drawScale
		// If it is greater than the drawScale paint a 20% alpha grey square on
		// this position.
		for (int i = 0; i < currentGrid.length; i++) {
			for (int k = 0; k < currentGrid[i].size(); k++) {
				//Do not paint dead civilisatoins
				if(currentCivilisations.get(k).isDead())
					continue;
				
				if (currentGrid[i].getPopulation(k) > getDrawScale()) {
					g.setColor(Color.black);
					g.setComposite(makeComposite(0.2f));
				} else {
					g.setColor(currentCivilisations.get(k).getColor());
					float alpha = (float) (currentGrid[i].getPopulation(k) *inverseDrawscale);
					// Important to catch any alpha out of range exceptions
					// incase of the maxscale not being changed.
					try {
						g.setComposite(makeComposite(alpha));
					} catch (Exception e) {
						g.setComposite(makeComposite(0.0f));
					}
				}

				// Paint the rectangle
				currentGrid[i].draw(g, xOffset, yOffset, getHeight());


			}
		}

	}

	@Override
	/**
	 * Overriden from the GridViewer class. Is used to find the maximum gridsite to scale to.
	 */
	public void calculateMaxScale(Site[] theGrid) {

		if (DRAWSCALE_FIXED)
			return;

		maxScale = 0.0;
		for (int i = 0; i < theGrid.length; i++) {

			if (theGrid[i].getTotal() > maxScale) {
				maxScale = theGrid[i].getTotal();
			}
		}
	}

}

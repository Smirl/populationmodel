package viewer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;


/**
 * A Purely Asthetical class which add the top bar to the GUI. It also holds the data the for the HELP button which is just added to a new frame
 * with it's visibility set to false;
 * @author s0941897
 *
 */
public class CustomHeaderBar extends JPanel {

	private static final long serialVersionUID = 8612404951125525149L;

	@SuppressWarnings("unused")
	private InteractivePopulation controller;

	/**
	 * The Main Title
	 */
	private JLabel heading;

	/**
	 * The help Frame
	 */
	public static JFrame help = new JFrame("Help");

	/**
	 * Contruct the header bar with the controller to attach to and the label for the title. Sets up the help window. Currently hard coded. 
	 * Not brililant but simple for an added feature. Ideally have a method to attach strings to the help widow. 
	 * @param attachedTo
	 * @param label
	 */
	public CustomHeaderBar(InteractivePopulation attachedTo, String label) {

		controller = attachedTo;
		heading = new JLabel(label);

		setOpaque(true);
		setBackground(Color.WHITE);
		setPreferredSize(new Dimension(600, 50));

		heading.setFont(new Font("Arial", Font.PLAIN, 18));
		heading.setForeground(new Color(221, 75, 57));

		this.setLayout(new GridLayout(1, 2));

		add(heading);

		help.setPreferredSize(new Dimension(400, 400));
		help.setLayout(new BorderLayout());
		help.setResizable(false);
		help.setVisible(false);
		help.pack();


		JLabel helpLabel = new JLabel("Help");
		final JTextArea info = new JTextArea();
		info.append(
				"On the single viewer, use the mouse to paint a map. Use to scroll wheel on the mouse to choose between"
				+"painting land or a species. Change the species to view/ paint on the left hand panel.\n\n");
				
		info.append("It is advised to alway use the Maximum drawing scale when running the simulation, this is the only drawing scale" 
				+" for the Single Viewer. The Use Max Scale checkbox refers to the all viewer. Uncheck this when paused allows you to " +
		"zoom into the population and look at lower maximum scale to draw for.\n\n");
		
		info.append("O: decrease D_people\n");
		info.append("P: increase D_people\n");
		info.append("Return: dump map to console\n");
		info.append("H: get this message\n");

		info.setLineWrap(true);
		info.setEditable(false);
		info.setWrapStyleWord(true);
		
		help.add(helpLabel,BorderLayout.NORTH);
		help.add(info, BorderLayout.CENTER);

		JButton showHelp = new JButton("Help");
		showHelp.setPreferredSize(new Dimension(80, 30));
		showHelp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				//JOptionPane.showMessageDialog(getParent(), info);
				if(help.isVisible()){
					help.setVisible(false);
				}else{
					help.setVisible(true);
					help.pack();
				}
			}
		});

		JPanel temp = new JPanel();
		temp.setBackground(Color.WHITE);
		temp.add(showHelp);
		
		add(temp);

		this.setBorder(BorderFactory.createMatteBorder(0, 0, 4, 0, new Color(
				200, 200, 200)));

		//heading.setBounds(10, 15, 300, 20);

	}
}

package viewer;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.Timer;

import main.data.Civilisation;
import main.data.Site;


/**
 * The abstract base class for all viewer which display a map. It has shared grid witdhs and abstract function which need to be over written.
 * It also contains default behaviour for some methods such as make Composite. 
 * @author s0941897
 *
 */
public abstract class GridViewer  extends JComponent implements MouseListener, MouseMotionListener, MouseWheelListener {


	/* **********************************
	 * VARIABLES
	 * ********************************** */
	private static final long serialVersionUID = 5060482423043239492L;

	//Create a controller
	protected InteractivePopulation controller;

	protected double drawScale = 10.0f;
	protected double maxScale = 10.0f;
	protected static boolean DRAWSCALE_CHANGED = false;
	protected static boolean DRAWSCALE_FIXED = false;

	protected int numberOfCivs;
	protected static int[] gridWidth = new int[2];

	//Things to paint
	public static int xOffset = 2723;
	public static int yOffset = -3080;
	private Point[] clicks = new Point[2];

	protected static main.data.Site[] currentGrid = null;
	protected static ArrayList<Civilisation> currentCivilisations = null; 
	protected static Color[] colours;
	protected static final Color WATER = new Color(100,183,255);
	protected static double scale = 1.0;



	//Painting timer which repaints
	/**
	 * The repaint timer, when calls the repaint
	 */
	protected Timer autoUpdater = new Timer(50, new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			repaint();
		}		
	});



	/* **********************************
	 * Constructor
	 * ********************************** */

	/**
	 * Create new panel to paint main simulation
	 * @param attachedTo the controller
	 */
	public GridViewer(InteractivePopulation attachedTo) {
		controller = attachedTo;
		setOpaque(true);
		setBackground(Color.WHITE);
		setPreferredSize(new Dimension(600,600));
		setSize(new Dimension(600,600));


		this.addMouseMotionListener(this);
		this.addMouseWheelListener(this);
		this.addMouseListener(this);
		
		try {
			initialise();
		} catch (Exception e) {}
	}

	/**
	 * Set the pointers to the grid, etc. for all of the GridViewers
	 */
	public void initialise() throws NullPointerException{
		currentGrid = controller.getCurrentGrid();
		numberOfCivs = controller.getNumberOfCivs();
		colours = new Color[numberOfCivs];
		for (int i = 0; i < numberOfCivs; i++) {
			colours[i] = Color.getHSBColor((float)i/(float)numberOfCivs, 1.0f, 0.5f);
		}
	}

	/**
	 * Makes an AlphaComposite with an opacity of <code>alpha</code>
	 * @param alpha
	 * @return The new AlphaComposite
	 */
	protected AlphaComposite makeComposite(float alpha) {
		int type = AlphaComposite.SRC_OVER;
		return(AlphaComposite.getInstance(type, alpha));
	}

	// ******* METHODS

	public void reset(){
		drawScale = 100.0f;
		maxScale = 100.0f;
		xOffset = 2723;
		yOffset = -3080;
		scale = 1.0;
	}

	//******** GETTERS

	/**
	 * Retruns the instance of the controller
	 * @return Controller
	 */
	protected InteractivePopulation getController(){
		return controller;
	}

	/**
	 * Get the scale to draw max alpha
	 * @return DrawScale
	 */
	protected double getDrawScale() {
		return drawScale;
	}

	//*********SETTERS

	@Override
	protected void paintComponent(Graphics g) {
		currentGrid = controller.getCurrentGrid();
		currentCivilisations = controller.getCurrentCivilisations();
	}

	public static void switchDrawMode(){
		DRAWSCALE_CHANGED = !DRAWSCALE_CHANGED;
	}

	public static void lockDrawMode(){
		DRAWSCALE_FIXED = !DRAWSCALE_FIXED;
	}

	/**
	 * Takes the double value of the slider from the controller class and if set to 100% uses the maximum drawScale otherwise 
	 */
	public void setDrawScale(double val) {
		if(DRAWSCALE_CHANGED == false) {
			System.out.println("Trying to change the draw scale when [Draw with max scale] is checked");
			System.exit(ERROR);
		}

		drawScale = maxScale*(float)val;

	}

	public void setMaxScale(double m){
		maxScale = m;
	}

	public double getMaxScale(){
		return maxScale;
	}

	/**
	 * sets the grid width for the viewer
	 */
	public static void setGridwidth(int w, int h){
		gridWidth[0] = w;
		gridWidth[1] = h;
	}


	/**
	 * Called when simulation is started to starts painting the grid at 50 millisecond intervals
	 */
	public void simulationStarted() {
		// Ensure display is up-to-date
		repaint();
		// Send repaint messages at regular intervals using the autoUpdater Timer object
		autoUpdater.restart();
	}

	/**
	 * Called when simulation is stopped, Stops timer and paints final state.
	 */
	public void simulationFinished() {
		// Stop sending regular repaint messages
		autoUpdater.stop();
		// Ensure display is up-to-date
		repaint();

	}

	/**
	 * Should be overwritten to calculate the maximum scale for that viewer. i.e. for one species all using the total density.
	 * @param theGrid The map of Sites to find the maximum of
	 */
	public abstract void calculateMaxScale(Site[] theGrid);

	//MouseWheelListener
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		scale += e.getWheelRotation()*0.1;
		if(scale < 0.1) scale=0.1;
		repaint();
	}

	//MouseMotionListener
	@Override
	public void mouseDragged(MouseEvent e) {
		clicks[1] = new Point(e.getX(), e.getY());
		xOffset -= (1.0/scale) * (int)(clicks[1].getX() - clicks[0].getX());
		yOffset -= (1.0/scale) * (int)(clicks[1].getY() - clicks[0].getY());
		
		clicks[0] = new Point(clicks[1]);
		this.repaint();
	}
	@Override
	public void mouseMoved(MouseEvent arg0) {	}

	//MouseListener
	@Override
	public void mouseClicked(MouseEvent e) {
	}
	@Override
	public void mouseEntered(MouseEvent e) {
	}
	@Override
	public void mouseExited(MouseEvent e) {
	}
	@Override
	public void mousePressed(MouseEvent e) {
		clicks[0] = new Point(e.getX(), e.getY());
	}
	@Override
	public void mouseReleased(MouseEvent e) {}

}

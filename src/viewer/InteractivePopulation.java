package viewer;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.HeadlessException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
//import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import main.Cartographer;
import main.Model;
import main.PopulationModel;
import main.data.Civilisation;
import main.data.Parameters;
import main.data.Site;

/**
 * The main JFrame of GUI. This is the Controller. All getting and setting is
 * controlled here and only this class communicates with the Model.
 * 
 * @author s0941897
 * 
 */
public class InteractivePopulation extends JFrame {

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {

				new InteractivePopulation();

			}
		});
	}

	// ~~~~~~~~~~~~~~~~~~~~~~ Constants ~~~~~~~~~~~~~~~~~~~~~
	/**
	 * The output directory root
	 */
	@SuppressWarnings("unused")
	private final static String OUTPUT_DIR = "output/";

	// ~~~~~~~~~~~~~~~~~~~~~ Variables ~~~~~~~~~~~~~~~~~~~~~

	private static final long serialVersionUID = -1525237176036159794L;

	private boolean running = false;


	// PPM output number
	/**
	 * Current PPM number. Reset by <code>reset()</code>
	 */
	@SuppressWarnings("unused")
	private int ppmNumber = 0;

	// Create a model
	/**
	 * The model used for all calculations. Allows for other models to be used
	 * if Model was abstract or extended
	 */
	protected Model m = null;

	// Create the viewing classes
	/**
	 * The All viewer panel. Kept as GridViewer type for general function calls
	 */
	private GridViewer allViewer = new AllViewer(this);
	/**
	 * The Single viewer panel. Kept as GridViewer type for general function
	 * calls
	 */
	private GridViewer singleViewer = new SingleViewer(this);

	/**
	 * The left hand control panel
	 */
	private PopUserPanel user = new PopUserPanel(this);

	/**
	 * The Carrying Capacity viewer
	 */
	private KViewer kViewer = new KViewer(this);

	/**
	 * Asthetic title bar
	 */
	protected CustomHeaderBar header = new CustomHeaderBar(this, "Population Diffusion");

	/**
	 * The tabbed pane to add the viewers to.
	 */
	protected JTabbedPane tab = new JTabbedPane();

	// Start a new thread which the simulation is started on
	/**
	 * The background thread for the model to run on
	 */
	private Thread background = null;

	// ~~~~~~~~~~~~~~~~~~~~~ Model Copy ~~~~~~~~~~~~~~~~~~~~~
	// A set of state variables stored when paused and to draw
	/**
	 * Local copy of the map when background thread doesn't exist. Can restart
	 * from current and more importantly keep painting.
	 */
	private Site[] theGrid = null;
	/**
	 * Local copy of the map when background thread doesn't exist. Can restart
	 * from current. S, D0, D
	 */
	private Parameters theParameters; // = {0.08, 0.04, 0.02, 0.06, 0.2, 0.2, 0.4,
	// 10.0, 500.0};

	private ArrayList<Civilisation> theCivilisations;

	// The interation number
	/**
	 * The current step number for calculating the actual time
	 */
	protected int stepNumber = 0;

	// ~~~~~~~~~~~~~~~~~~~~~ Constructors ~~~~~~~~~~~~~~~~~~~~~

	/**
	 * The constructor which adds the JPanels to the frame and sets up an
	 * initial condition
	 * 
	 * @throws HeadlessException
	 * @throws IOException
	 */
	public InteractivePopulation(){
		super("Fall of Ancient Civilisations");
		init();
	}

	/**
	 * Called by the constructors, The main set up of the JFrame
	 * 
	 * @throws IOException
	 */
	public void init() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		//Add the basic panels to the tab pane. 
		addPanels();

		getContentPane().add(tab, BorderLayout.CENTER);
		getContentPane().add(user, BorderLayout.WEST);
		getContentPane().add(header, BorderLayout.NORTH);
		pack();

		tab.addKeyListener(Keyboard.listener);
		tab.setFocusable(true);
		// Sets the model and zeros timers ect.
		reset();

		// Show and paint the Main JFrame
		setVisible(true);

	}

	// ~~~~~~~~~~~~~~~~~~~~~ Getters ~~~~~~~~~~~~~~~~~~~~~
	/**
	 * Gets the current state from the model to give to the viewer. Also updates
	 * the in-class private variables used for keeping track of paused
	 * simulations
	 * 
	 * @return the current grid
	 */
	public Site[] getCurrentGrid() {

		if (m != null) {
			synchronized (m) {

				theGrid = m.copyMap();
				try {
					((GridViewer) tab.getSelectedComponent()).calculateMaxScale(theGrid);
				} catch (ClassCastException e) {
				}
			}
		}
		return theGrid;
	}

	/**
	 * Gets the current state of the double parameters (Rates) to give to the
	 * viewer.; Also updates the in-class private variables used for keeping
	 * track of paused simulations.
	 * 
	 * @return the current parameters
	 */
	public Parameters getCurrentRates() {

		synchronized (m) {
			theParameters = m.getParameters();
		}

		return theParameters;
	}

	public ArrayList<Civilisation> getCurrentCivilisations(){
		synchronized (m) {
			theCivilisations = m.getCivilisations();
		}

		return theCivilisations;
	}

	/**
	 * Returns the current Model that the controler is viewing
	 * 
	 * @return
	 */
	public Model getModel() {
		return m;
	}

	/**
	 * Gets the current number of Civilisations on the first, and therefore all,
	 * sites.
	 * 
	 * @return the number of civilisations
	 */
	public int getNumberOfCivs() {

		if (theGrid != null)
			return Model.model.getCivilisations().size();
		else
			return 0;
	}


	// ~~~~~~~~~~~~~~~~~~~~~ Setters ~~~~~~~~~~~~~~~~~~~~~

	/**
	 * Set the draw scale for the current viewer. Called by the UserPanel.
	 * 
	 */
	public void setDrawScale(double val) {
		((GridViewer) tab.getSelectedComponent()).setDrawScale(val);
	}

	/**
	 * Set the MAX scale for the userpanel AND MOVE the SLIDER
	 * 
	 */
	public void setUserSliderScale(double max) {
		user.setScale(max);
	}

	/**
	 * Just change the label for the max scale
	 */
	public void setUserSliderLabel(double max) {
		user.setDrawLabel(max);
	}


	// ~~~~~~~~~~~~~~~~~~~~~ Methods ~~~~~~~~~~~~~~~~~~~~~
	/**
	 * increases the SingleViewers species index by one. SingleViewer deals with
	 * outofbounds
	 */
	public void changeCIV(int civ) {
		((SingleViewer) singleViewer).changeView(civ);
	}

	/**
	 * Changes the draw mode from maximum scale to a custom set scale.
	 */
	public void switchDrawMode() {
		GridViewer.switchDrawMode();
	}

	/**
	 * Switches from allowing the drawscale to change, to a fixed mode
	 */
	public void lockDrawMode() {
		GridViewer.lockDrawMode();
	}

	/**
	 * Is the simulation running?
	 * 
	 * @return T/F
	 */
	public boolean running() {
		if (background == null)
			return false;
		else
			return true;
	}

	/**
	 * Reset the grid from files if they exist.
	 * 
	 * @throws IOException
	 */
	public void reset(){
		stepNumber = 0;
		ppmNumber = 0;

		allViewer.reset();
		singleViewer.reset();
		kViewer.reset();

		createModel();

		getCurrentGrid();
		getCurrentRates();
		getCurrentCivilisations();

		// Get drawScale for all Gridviewers
		for (Component i : tab.getComponents()) {
			try {
				((GridViewer) i).initialise();
				((GridViewer) i).calculateMaxScale(theGrid);
			} catch (ClassCastException e) {

			}
		}

		repaint();
	}

	/**
	 * Step the simulation one step
	 */
	public void step(){
		m.simulationStep();
		stepNumber++;
		user.setStepNumberLabel(stepNumber);
	}

	/**
	 * Create the model to be used on reset() and init(). Allows for the InteractivePopulation to work with any model.
	 */
	protected void createModel() {
		Model.model = null; 

		int mapseed = new Random().nextInt();
		//m = new PopulationModel(Cartographer.generateIslands(mapseed, 0.3, 5, 300.0, 3));
		//m = new PopulationModel(Cartographer.generateSquare(mapseed, 100, 1, false));
		//m = new PopulationModel(Cartographer.generateCenter(mapseed, 100));
		m = new PopulationModel(Cartographer.generateArena(mapseed, 100, true));
		m.getCivilisations().get(0).setStrategy(1.0, 0.0, 0.0);
		m.getCivilisations().get(1).setStrategy(0.0, 0.0, 1.0);

	}

	/**
	 * Add the single, all, and K viewers to the tab pane.
	 */
	protected void addPanels(){
		tab.addTab("SINGLE", null, singleViewer, "Shows only Hares or Pumas");
		tab.addTab("ALL", null, allViewer, "See Hares and Pumas");
		tab.addTab("K", null, kViewer, "Shows the carrying capacity");
	}

	// ~~~~~~~~~~~~~~~~~~~~~ Start and Stop ~~~~~~~~~~~~~~~~~~~~~

	/**
	 * Starts the simulation on the background thread.
	 */
	public void startSimulation() {

		if (background != null)
			return;
		running = true;

		// Set up a new simulation with the desired initial condition (since
		// this may be different from the state in which the last simulation
		// ended)
		//m = new PopulationModel(theGrid, theCivilisations, theMatrix, theRates);

		// Run it in a new background thread
		background = new Thread() {
			@Override
			public void run() {

				// Communicate with the view objects on the Swing thread
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						// Inform the view objects that the simulation has
						// started

						allViewer.simulationStarted();
						singleViewer.simulationStarted();
						kViewer.simulationStarted();
						user.simulationStarted();
					}
				});

				// Repeatedly iterate the equations of motion until the thread
				// is interrupted
				while (running) {
					step();
				}

				// Communicate with the view objects on the Swing thread
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						// Remove the reference to the background thread (note,
						// we do this
						// on the Swing thread to avoid concurrency problems)
						background = null;
						// Inform the view objects that the simulation has
						// finished
						allViewer.simulationFinished();
						singleViewer.simulationFinished();
						kViewer.simulationFinished();
						user.simulationStopped();
					}
				});

			}
		};

		background.start();
	}

	/**
	 * Interupts the thread and stops the timers from running.
	 */
	public void stopSimulation() {
		// Check the a background thread is actually running
		if (background == null)
			return;
		// Send the interrupt message to the background thread to stop it at a
		// convenient point
		background.interrupt();
		running = false;
		// Update all vairables to the current state
		getCurrentGrid();
		getCurrentRates();
	}

}

package viewer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import main.data.Site;

public class KViewer extends GridViewer  {

	private static final long serialVersionUID = 5563111557677501472L;


	public KViewer(InteractivePopulation attachedTo) {
		super(attachedTo);
	}

	protected void paintComponent(Graphics gg) {
		
		super.paintComponent(gg);

		Graphics2D g = (Graphics2D) gg;
		

		// Clear the background if we are an opaque component
		g.setColor(getBackground());
		g.fillRect(0, 0, getWidth(), getHeight());
		g.setColor(Color.PINK);
		g.drawString("Maximum Density Visible (Drawscale): " + maxScale + " xOffset: "+xOffset+ " yOffset: "+yOffset, 20, 20);

		g.scale(scale, scale);
		
		if (!DRAWSCALE_CHANGED) {
			drawScale = maxScale;
			getController().setUserSliderLabel(maxScale);
		} else {
			getController().setUserSliderScale(maxScale);
		}
		
		g.setColor(Color.BLACK);

		// Set correct colour and then paint a rectange for each site
		for (int i = 0; i < currentGrid.length; i++) {

			float alpha = (float) currentGrid[i].getK() / (float) maxScale;

			try{
				g.setComposite(makeComposite(alpha));
			}catch(Exception e){
				g.setComposite(makeComposite(0.0f));
			}

			currentGrid[i].draw(g, xOffset, yOffset, getHeight());

		}

	}

	@Override
	public void calculateMaxScale(Site[] theGrid) {
		if (DRAWSCALE_FIXED)
			return;
		
		maxScale = 0.0;
		for (int i = 0; i < currentGrid.length; i++) 
			if (currentGrid[i].getK() > maxScale) {
				maxScale = currentGrid[i].getK();
			//	System.out.println("KVIEWER_MAXSCALE: "+maxScale);
			}
	}
}

/**
 * 
 */
package viewer;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import main.Model;

import static java.awt.event.KeyEvent.*;

/**
 * @author s0941897
 *
 */
public class Keyboard {
	
	public static KeyListener listener = new KeyListener() {
		
		@Override
		public void keyTyped(KeyEvent e) {
			//Do nothing
		}
		
		@Override
		public void keyReleased(KeyEvent e) {
			//Do nothing
		}
		
		@Override
		public void keyPressed(KeyEvent e) {
			System.out.println("Key Pressed");
			if(e.getKeyCode() == VK_ENTER){
				Model.model.dumpMap();
			}
			if(e.getKeyCode() == VK_H){
			
				CustomHeaderBar.help.setVisible(true);
					
			}
			
		}
	};

	
	
	
}

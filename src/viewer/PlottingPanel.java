/**
 * 
 */
package viewer;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

import main.data.Civilisation;

/**
 * @author s0941897
 *
 */
public class PlottingPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private static final int SPACING = 2;
	private static final double max = 1000000.0;
	
	private ArrayList<Double> previous;
	private ArrayList<Double> next;
	private ArrayList<Civilisation> civilisations;
	private boolean clean;
	private int loc;
	
	
	
	public PlottingPanel() {
		setBackground(Color.WHITE);
		previous = new ArrayList<Double>();
		next = new ArrayList<Double>();

		clear();
	}
	
	public void add(ArrayList<Civilisation> civs, double[] totals){
		civilisations = civs;
		previous.clear();
		previous.addAll(next);
		
		next.clear();
		for (int i = 0; i < totals.length; i++) {
			next.add(totals[i]);
		}
		
		repaint();
	}
	
	public void clear(){
		clean = true;
		previous.clear();
		next.clear();
		loc=0;
		repaint();
		clean=false;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(clean){
			g.setColor(getBackground());
			g.fillRect(0, 0, getWidth(), getHeight());
		}
		
		loc += SPACING;
		
		for (int i = 0; i < previous.size(); i++) {
			g.setColor(civilisations.get(i).getColor());
			System.out.println(loc+" "+ (int)(previous.get(i)*getHeight()/max) +" "+ (loc+SPACING) +" "+ (int)(next.get(i)*getHeight()/max));
			g.drawLine(loc, (int)(previous.get(i)*getHeight()/max), loc+SPACING, (int)(next.get(i)*getHeight()/max));
		}
	}
	
}

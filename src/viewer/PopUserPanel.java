package viewer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.Hashtable;

//import javax.media.j3d.J3DGraphics2D;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

//import main.InteractivePopulation;

/**
 * The left hand user panel for starting the simulatin etc. Adds many JComps to the panel with actionlisteners for functionallity. 
 */
public class PopUserPanel extends JPanel {

	private static final long serialVersionUID = 446404881175167381L;

	/**
	 * The controller to attach to
	 */
	private InteractivePopulation controller;

	/**
	 * The Current civilisation to view. MUST BE ZERO TO MATCH SINGLE VIEWER AND AVOID NULL POINTER
	 * @see SingleViewer CIV
	 */
	private int currentCiv = 0;

	/**
	 * The deafult max value. 
	 */
	private double maxValue = 10.0;

	//*****************************************
	// Components
	//*****************************************

	private JButton start = new JButton("START");
	private JButton reset = new JButton("RESET");
	private JButton step = new JButton("STEP");
	private JLabel countS = new JLabel("Time: 0000");
	private JButton civUp = new JButton("Next Civilisation");
	private JCheckBox useMaxDrawscale = new JCheckBox("Use  Max Scale?", true);
	private JCheckBox lockDrawscale = new JCheckBox("Lock Max Scale?", false);
	private JSlider drawScale = new JSlider(JSlider.VERTICAL, 1, 100, 100);
	private JFormattedTextField drawLabel = new JFormattedTextField(new DecimalFormat("###.0000"));
	private JLabel[] helperLabels = new JLabel[3];
	private JButton aboutCivsButton = new JButton("About current Civs");
	private JTextArea aboutCivs = new JTextArea(); 

	private Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer,JLabel>();

	private JFrame civFrame = new JFrame("Current Civilisations");

	// **************************************
	//Colors
	//***************************************

	//set up a bunch of colours for making things look nice
	public static Color redRidingHood = new Color(209,72,54);
	public Color darkGray = new Color(50, 50, 50);
	public Color lightGray = new Color(250,250,250);
	public Color gray = new Color(200,200,200);

	/**
	 * Adds the componets to the panel and sets up action listeners 
	 * @param attachedTo
	 */
	public PopUserPanel(InteractivePopulation attachedTo) {

		controller = attachedTo;
		setOpaque(true);
		setBackground(Color.WHITE);
		//Size of the left hand user panel
		setPreferredSize(new Dimension(150,600));

		civFrame.setAlwaysOnTop(true);
		civFrame.setPreferredSize(new Dimension(500,500));
		civFrame.setLayout(new BorderLayout());
		civFrame.add(new JLabel("Current Civilisations"), BorderLayout.NORTH);
		civFrame.add(aboutCivs,BorderLayout.CENTER);
		civFrame.pack();
		aboutCivs.setEditable(false);


		start.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(controller.running() == true){
					controller.stopSimulation();
				}else{
					controller.startSimulation();
				}

			}
		});

		reset.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(controller.running() == false){
					controller.reset();
				}

			}
		});

		step.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(controller.running() == false){
					controller.step();
					controller.repaint();
				}

			}
		});
		//Change the civ + on press of Return Key
		civUp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Total Civs: "+GridViewer.currentCivilisations.size());
				currentCiv = (currentCiv + 1)%GridViewer.currentCivilisations.size();
				controller.changeCIV(currentCiv);	
				controller.repaint();

			}
		});

		useMaxDrawscale.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(controller.running() == false){
					controller.switchDrawMode();
					drawScale.setEnabled(!useMaxDrawscale.isSelected());
				}else{
					useMaxDrawscale.setSelected(!useMaxDrawscale.isSelected());
				}
			}
		});

		lockDrawscale.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(controller.running() == false){
					controller.lockDrawMode();
				}else{
					lockDrawscale.setSelected(!lockDrawscale.isSelected());
				}
			}
		});

		drawScale.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				double val = (double)drawScale.getValue()/100.0;
				controller.setDrawScale(val);
				drawLabel.setText(val*maxValue +"");
				controller.repaint();
			}
		});

		drawLabel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.setDrawScale(Double.parseDouble(drawLabel.getText()));
				controller.repaint();
			}
		});

		aboutCivsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				aboutCivs.setText("");
				for (int i = 0; i < GridViewer.currentCivilisations.size(); i++) {
					aboutCivs.append(GridViewer.currentCivilisations.get(i).toString()+"\n");
				}
				civFrame.setVisible(true);
			}
		});

		//Start-stop button style including dimensions and colours
		start.setBackground(redRidingHood);
		start.setForeground(Color.WHITE);
		start.setPreferredSize(new Dimension(130, 35));
		start.setFocusable(false);

		//Reset button style including dimensions and colours
		reset.setBackground(darkGray);
		reset.setPreferredSize(new Dimension(130,35));
		reset.setForeground(Color.WHITE);
		reset.setFocusable(false);

		//Reset button style including dimensions and colours
		step.setBackground(darkGray);
		step.setPreferredSize(new Dimension(130,35));
		step.setForeground(Color.WHITE);
		step.setFocusable(false);

		//aboutCiv button stle
		aboutCivsButton.setBackground(darkGray);
		aboutCivsButton.setPreferredSize(new Dimension(130,35));
		aboutCivsButton.setForeground(Color.WHITE);
		aboutCivsButton.setFocusable(false);

		//Number of steps label size
		countS.setPreferredSize(new Dimension(130, 35));

		//Random scheme button style including dimensions, text and colours
		civUp.setBackground(lightGray);
		civUp.setPreferredSize(new Dimension(130, 35));
		civUp.setFont(new Font("Arial", Font.BOLD, 10));
		civUp.setMargin(new Insets(0, 0, 0, 0));
		civUp.setFocusable(false);
		civUp.setToolTipText("Change which civilisation to look at");

		useMaxDrawscale.setFont(new Font("Arial", Font.BOLD, 9));

		lockDrawscale.setFont(new Font("Arial", Font.BOLD, 9));

		//Label Table
		//Create the label table for the sliders

		labelTable.put( new Integer( 0 ), new JLabel("0") );
		labelTable.put( new Integer( 100 ), new JLabel("1") );



		//Scale slider
		drawScale.setEnabled(false);

		drawScale.setLabelTable( labelTable );
		drawScale.setPaintLabels(true);
		drawScale.setMajorTickSpacing(10);
		drawScale.setPaintTicks(true);
		drawScale.setPreferredSize(new Dimension(130, 150));
		drawScale.setBackground(Color.WHITE);
		drawScale.setForeground(redRidingHood);

		//label prefs
		drawLabel.setPreferredSize(new Dimension(130, 20));
		drawLabel.setText(maxValue+"");
		drawLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 9));

		//who is who panel prefs

		//Set helper labels
		helperLabels[0] = new JLabel("Single View Options");
		helperLabels[1] = new JLabel("All View Options");
		helperLabels[2] = new JLabel("Legend");

		//more things here

		add(start);
		add(reset);
		add(step);
		add(countS);

		//Single Viewer Controls
		JPanel single = new JPanel();
		single.setBackground(gray);
		single.setPreferredSize(new Dimension(140, 70));
		single.add(helperLabels[0]);
		single.add(civUp);
		add(single);

		//All viewer commands
		JPanel all = new JPanel();
		all.setBackground(gray);
		all.setPreferredSize(new Dimension(140, 260));
		all.add(helperLabels[1]);
		all.add(useMaxDrawscale);
		all.add(lockDrawscale);
		all.add(drawLabel);
		all.add(drawScale);
		add(all);

		add(helperLabels[2]);
		add(aboutCivsButton);

	}

	/**
	 * Set the drawlabel value
	 */
	public void setDrawLabel(double max){
		drawLabel.setText(max+"");
	}

	/**
	 * Set the number of steps so far in the user panel
	 * 
	 * @param n The number of steps to display
	 */
	public void setStepNumberLabel(double n){
		countS.setText("Time: "+n);
	}

	/**
	 * Get the max value for the scale slider from the controller
	 */
	public void setScale(double max){
		this.drawScale.setValue((int)((double)drawScale.getValue()*maxValue/max));
		maxValue = max;
		labelTable.get(100).setText((int)maxValue+"");
	}

	/**
	 * Called when simulation started to change most elements to non editable. Changes start-stop text
	 * and gridsize in case user has not pressed enter on gridsize (to avoid confusion)
	 */
	public void simulationStarted(){
		start.setText("STOP");
		reset.setEnabled(false);
		step.setEnabled(false);
		useMaxDrawscale.setEnabled(false);
		lockDrawscale.setEnabled(false);
		drawScale.setEnabled(false);
	}

	/**
	 * Called when simulation is stopped to change most elements to editable. Changes start-stop text
	 * 
	 */
	public void simulationStopped(){
		start.setText("START");
		reset.setEnabled(true);
		step.setEnabled(true);
		useMaxDrawscale.setEnabled(true);
		lockDrawscale.setEnabled(true);
		if(useMaxDrawscale.isSelected() == false) drawScale.setEnabled(true);

	}
}

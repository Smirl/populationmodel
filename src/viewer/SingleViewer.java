package viewer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;

import main.Model;
import main.data.Site;

/**
 * The GridViewer which displays one species using AlphaComposites. The Colours
 * are calculated in the same way as AllViewer to Avoid confusion.
 */
public class SingleViewer extends GridViewer implements MouseMotionListener, MouseWheelListener, MouseListener {

	private static final long serialVersionUID = -7099883002991529183L;

	// ~~~~~~~~~~~~~~~~~~~~~ variables ~~~~~~~~~~~~~~~~~~~~~

	/**
	 * The current species/ civilisation to view MUST BE ZERO AT START
	 * 
	 * @see PopUserPanel
	 */
	private int CIV = 0;

	// ~~~~~~~~~~~~~~~~~~~~~ Constructors ~~~~~~~~~~~~~~~~~~~~~
	/**
	 * @param attachedTo
	 */
	public SingleViewer(InteractivePopulation attachedTo) {
		super(attachedTo);
	}

	// ~~~~~~~~~~~~~~~~~~~~~ Methods ~~~~~~~~~~~~~~~~~~~~~
	/**
	 * Changes the civilsiation being looked at in the SingleViewer
	 * 
	 * @param civ
	 */
	public void changeView(int civ) {
		CIV = civ;
		System.out.println("Civilisation Index: " +CIV);
	}

	protected void paintComponent(Graphics gg) {

		super.paintComponent(gg);

		Graphics2D g = (Graphics2D) gg;

		// Clear the background if we are an opaque component
		g.setColor(getBackground());
		g.fillRect(0, 0, getWidth(), getHeight());

		g.setColor(Color.PINK);
		g.drawString("Maximum Density Visible (Drawscale): " + maxScale + " xOffset: "+xOffset+ " yOffset: "+yOffset, 20, 20);

		g.scale(scale, scale);

		getController().setUserSliderLabel(maxScale);

		// Set correct colour and then paint a rectange for each site
		if (currentGrid != null) {
			for (int i = 0; i < currentGrid.length ; i++) {
				if(CIV>=currentGrid[i].size())
					continue;
				g.setColor(Model.model.getCivilisations().get(CIV).getColor());
				float alpha = (float) (currentGrid[i].getPopulation(CIV) / maxScale);
				try {
					g.setComposite(makeComposite(alpha));
				} catch (Exception e) {
					g.setComposite(makeComposite(0.0f));
				}

				currentGrid[i].draw(g, xOffset, yOffset, getHeight());

			}
		}

	}

	@Override
	public void calculateMaxScale(Site[] theGrid) {
		if (DRAWSCALE_FIXED)
			return;
		maxScale = 0.0;

		for (int i = 0; i < theGrid.length; i++) {
			if(CIV>=theGrid[i].size())
				continue;
			if (theGrid[i].getPopulation(CIV) > maxScale) {
				maxScale = theGrid[i].getPopulation(CIV);
			}

		}

	}

}
